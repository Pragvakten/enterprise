<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Video</title>
<link href="<%=request.getContextPath()%>/main.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
	<div class="container">
		<div class="row">
			<jsp:include page="header.jsp"></jsp:include>
		</div>
		<div class="row">
			<iframe id="videoPlayer" width="480" height="390"
				src="//www.youtube.com/embed/${id}" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
</body>
</html>