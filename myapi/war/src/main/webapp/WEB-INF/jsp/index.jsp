<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>PlusTube</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/main.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div id="container">
		<div class="row">
			<jsp:include page="header.jsp"></jsp:include>
		</div>
		<div class="row">
			<form id="searchForm"
				action="<%=request.getContextPath()%>/index.html" method="get">
				<div class="form-div">
					<input id="searchField" type="text" maxlength="255" name="q"
						required="required" />
				</div>
				<div class="form-div">
					<input id="searchButton" type="submit" value="Search" />
				</div>
				<div class="form-div">
					<a id="oauthLink" href="<%= request.getContextPath() %>/index.html?auth">Show Oauth token</a>
				</div>
				<div class="form-div">
					<span>${token}</span>
				</div>
			</form>
		</div>
		<!-- show data -->
		<div class="row">
			<c:forEach items="${results}" var="result">
				<div class="col-md">
					<div class="thumb-img">
						<a href="<%= request.getContextPath() %>/video.html?id=${result.videoId}" target="_blank"> <img
							alt="thumbnail" src="${result.thumbnailUrl}" />
						</a>
					</div>
					<div>
						<h3 class="video-title">${result.title}</h3>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>