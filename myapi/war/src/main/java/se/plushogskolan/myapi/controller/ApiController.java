package se.plushogskolan.myapi.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.myapi.domain.video.Video;
import se.plushogskolan.myapi.integration.service.VideoSearchService;

@Controller
public class ApiController {

	@Inject
	private VideoSearchService searchService;

	@ResponseBody
	@RequestMapping(value = "/api/search/{q}", method = RequestMethod.GET, produces = "application/json")
	public List<Video> jsonSearch(@PathVariable String q) {
		return searchService.getVideosFromYouTube(q);
	}
}
