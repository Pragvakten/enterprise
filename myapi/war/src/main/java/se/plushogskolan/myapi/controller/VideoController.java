package se.plushogskolan.myapi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/video.html")
public class VideoController {

	@RequestMapping(method = RequestMethod.GET, params = { "id" })
	public ModelAndView showVideo(@RequestParam String id) {
		ModelAndView view = new ModelAndView("video");

		view.addObject("id", id);
		return view;
	}
}
