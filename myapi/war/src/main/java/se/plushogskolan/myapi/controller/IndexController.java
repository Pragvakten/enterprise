package se.plushogskolan.myapi.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.myapi.domain.video.Video;
import se.plushogskolan.myapi.integration.service.OauthService;
import se.plushogskolan.myapi.integration.service.VideoSearchService;

@Controller
@RequestMapping(value = "/index.html")
public class IndexController {

	@Inject
	private VideoSearchService searchService;
	@Inject
	private OauthService oAuthService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getIndex() {
		ModelAndView view = new ModelAndView("index");
		return view;
	}

	@RequestMapping(method = RequestMethod.GET, params = { "q" })
	public ModelAndView search(@RequestParam String q) {
		ModelAndView view = new ModelAndView("index");
		List<Video> videos = searchService.getVideosFromYouTube(q);

		view.addObject("results", videos);
		return view;
	}

	@RequestMapping(method = RequestMethod.GET, params = { "auth" })
	public void oAuth(@RequestParam String auth, HttpServletRequest req,
			HttpServletResponse res) throws IOException {

		res.sendRedirect(oAuthService.getProfileAuthUrl());
	}

	@RequestMapping(method = RequestMethod.GET, params = { "code" })
	public ModelAndView oAuthCode(@RequestParam String code) throws IOException {
		ModelAndView view = new ModelAndView("index");
		view.addObject("token", oAuthService.getTokenData(code));

		return view;
	}
}
