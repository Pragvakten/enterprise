package se.plushogskolan.myapi.integration.service;

import java.io.IOException;
import java.util.Arrays;

import javax.ejb.Stateless;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;

@Stateless
public class OauthServiceImpl implements OauthService {

	public static final String REDIRECT_URI = "http://localhost:8080/myapi/index.html";
	public static final String CLIENT_SECRET = "HX8r0RjuG6hzqi9OQi6IY0zN";
	public static final String CLIENT_ID = "819913064395-5mtbdvhh5vp2d8ioio27qmr8b4l98s10.apps.googleusercontent.com";
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	private static YouTube youtube;

	@Override
	public String getTokenData(String code) throws IOException {
		GoogleAuthorizationCodeFlow authorizationCodeFlow = new GoogleAuthorizationCodeFlow(
				HTTP_TRANSPORT,
				JSON_FACTORY,
				CLIENT_ID,
				CLIENT_SECRET,
				Arrays.asList(
						"https://www.googleapis.com/auth/yt-analytics.readonly",
						"https://www.googleapis.com/auth/youtube.readonly"));

		GoogleTokenResponse response = authorizationCodeFlow
				.newTokenRequest(code).setRedirectUri(REDIRECT_URI).execute();

		return response.getAccessToken();
	}

	@Override
	public String getProfileAuthUrl() {
		return new GoogleAuthorizationCodeRequestUrl(CLIENT_ID, REDIRECT_URI,
				Arrays.asList("profile")).build();
	}
}
