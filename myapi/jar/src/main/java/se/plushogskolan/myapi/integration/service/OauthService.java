package se.plushogskolan.myapi.integration.service;

import java.io.IOException;

public interface OauthService {

	String getProfileAuthUrl();

	String getTokenData(String code) throws IOException;
}
