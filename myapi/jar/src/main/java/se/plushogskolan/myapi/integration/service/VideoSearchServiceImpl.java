package se.plushogskolan.myapi.integration.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;

import se.plushogskolan.myapi.domain.video.Video;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;

@Stateless
public class VideoSearchServiceImpl implements VideoSearchService {

	private final static long NUM_VIDEOS_RETURNED = 10;
	private final static String APP_NAME = "ultimate-karma-727";
	private final static String API_KEY = "AIzaSyDboUtwvMQWjUxHRUCvpy_AdNXzKwuVc-c";
	private final static HttpTransport TRANSPORT = new NetHttpTransport();
	private final static JsonFactory JSON_FACTORY = new JacksonFactory();
	private static YouTube youTube;

	@Override
	public List<Video> getVideosFromYouTube(String query) {
		youTube = new YouTube.Builder(TRANSPORT, JSON_FACTORY,
				new HttpRequestInitializer() {

					@Override
					public void initialize(HttpRequest arg0) throws IOException {

					}
				}).setApplicationName(APP_NAME).build();

		try {
			YouTube.Search.List search = youTube.search().list("id, snippet");
			search.setKey(API_KEY);
			search.setQ(query);
			search.setType("video");
			search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
			search.setMaxResults(NUM_VIDEOS_RETURNED);

			SearchListResponse response = search.execute();
			return mapResultsToVideo(response.getItems().iterator());

		} catch (IOException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	private List<Video> mapResultsToVideo(Iterator<SearchResult> iterator) {
		List<Video> result = new ArrayList<Video>();

		while (iterator.hasNext()) {
			SearchResult searchResult = iterator.next();

			if (searchResult.getId().getKind().equals("youtube#video")) {
				result.add(convertToVideo(searchResult));
			}
		}
		return result;
	}

	private Video convertToVideo(SearchResult searchResult) {
		Thumbnail thumbnail = searchResult.getSnippet().getThumbnails()
				.getDefault();

		return new Video(searchResult.getId().getVideoId(), searchResult
				.getSnippet().getTitle(), thumbnail.getUrl());
	}
}
