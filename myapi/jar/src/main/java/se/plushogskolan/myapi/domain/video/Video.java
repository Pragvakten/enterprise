package se.plushogskolan.myapi.domain.video;

/**
 * Represents a youtube video
 * 
 * @author fidde
 * 
 */
public class Video {

	private String videoId;
	private String title;
	private String thumbnailUrl;

	public Video(String videoId, String title, String thumbnailUrl) {
		setVideoId(videoId);
		setTitle(title);
		setThumbnailUrl(thumbnailUrl);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
}
