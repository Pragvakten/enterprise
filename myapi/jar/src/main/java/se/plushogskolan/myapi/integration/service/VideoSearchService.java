package se.plushogskolan.myapi.integration.service;

import java.util.List;

import se.plushogskolan.myapi.domain.video.Video;

public interface VideoSearchService {

	List<Video> getVideosFromYouTube(String query);
}
