package se.plushogskolan.cdi.services;

import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Enviroment;
import se.plushogskolan.cdi.annotations.EnviromentEnum;
import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.IncomeEnum;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.model.Owner;
import se.plushogskolan.cdi.repository.CarRepository;

@Income(IncomeEnum.MEDIUM)
public class NormalCarService implements CarService {

	/**
	 * Injecting a medium income owner. The Owner is created using the @Producer
	 * method in Owner.java.
	 */
	@Inject
	@Income(IncomeEnum.MEDIUM)
	private Owner owner;

	@Inject
	@Enviroment(EnviromentEnum.MOCK)
	private CarRepository carRepository;

	@InvocationCounter
	public Car getCar() {
		Car car = carRepository.getNormalCar();
		car.setOwner(owner);
		return car;
	}

	public void saveCar(Car car) {
		carRepository.saveCar(car);

	}

	@Override
	public String toString() {
		return "NormalCarService";
	}

}
