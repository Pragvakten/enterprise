package se.plushogskolan.cdi.repository.db;

import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Enviroment;
import se.plushogskolan.cdi.annotations.EnviromentEnum;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.repository.CarRepository;

/**
 * A repository to be used in the production environment
 * 
 */
@Enviroment(EnviromentEnum.PROD)
public class DbCarRepository implements CarRepository {

	@Inject
	Event<Car> carEvent;

	public DbCarRepository() {
	}

	@InvocationCounter
	public Car getNormalCar() {
		return new Car("DB normal car");
	}

	@InvocationCounter
	public Car getSportsCar() {
		return new Car("DB sports car");
	}

	public void saveCar(Car car) {
		System.out.println("Save car in DB: " + car.getName());
		carEvent.fire(car);
	}

}
