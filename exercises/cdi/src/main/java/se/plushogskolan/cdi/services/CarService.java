package se.plushogskolan.cdi.services;

import se.plushogskolan.cdi.model.Car;

public interface CarService {

	Car getCar();

	void saveCar(Car car);
}
