package se.plushogskolan.cdi.decorators;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.IncomeEnum;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.services.CarService;

/**
 * A decorator that intercepts method calls to a CarService mapped with income =
 * HIGH. Will change the name when the getCar() method is called.
 * 
 */
@Decorator
public abstract class SportCarDecorator implements CarService {

	@Inject
	@Income(IncomeEnum.HIGH)
	@Delegate
	CarService carService;

	public Car getCar() {
		System.out.println("SportCarDecorator.getCar() is invoked");
		Car car = carService.getCar();
		car.setName(car.getName() + " with some decorated wrom wrom");
		return car;
	}

}
