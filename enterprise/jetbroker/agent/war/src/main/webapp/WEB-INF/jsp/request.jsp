<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Flight requests</title>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.css"
	type="text/css" rel="stylesheet">
</head>
<body>
	<jsp:include page="header.jsp" />
	<div>
		<h2><spring:message code="flightrequest.header"></spring:message></h2>
		<hr>
	</div>
	<div id="formDiv">
		<form:form commandName="flightRequestBean" method="POST"
			action="/request.html">
			<form:errors class="errors" path=""></form:errors>
			<div>
				<form:input path="id" type="hidden" />
				<form:label path="userId">
					<spring:message code="global.customer"></spring:message>
				</form:label>
				<form:select path="userId">
					<form:option value=""></form:option>
					<form:options items="${customers }" itemValue="id"
						itemLabel="fullName" />
				</form:select>
				<form:errors path="userId" class="errors"></form:errors>
			</div>
			<div class="clear">
				<form:label path="departureAirport">
					<spring:message code="flightrequest.departureAirport"></spring:message>
				</form:label>
				<form:select path="departureAirport">
					<form:option value=""></form:option>
					<form:options items="${airports}" itemLabel="niceName"
						itemValue="code" />
				</form:select>
				<form:errors path="departureAirport" class="errors"></form:errors>
			</div>
			<div class="clear">
				<form:label path="arrivalAirport">
					<spring:message code="flightrequest.arrivalAirport"></spring:message>
				</form:label>
				<form:select path="arrivalAirport">
					<form:option value=""></form:option>
					<form:options items="${airports}" itemLabel="niceName"
						itemValue="code" />
				</form:select>
				<form:errors path="arrivalAirport" class="errors"></form:errors>
			</div>
			<div class="clear">
				<form:label path="numPassangers">
					<spring:message code="flightrequest.noOfPassengers"></spring:message>
				</form:label>
				<form:input path="numPassangers" type="number" />
				<form:errors path="numPassangers" class="errors"></form:errors>
			</div>
			<div class="clear">
				<form:label path="departureDate">
					<spring:message code="flightrequest.departureDate"></spring:message>
				</form:label>
				<form:input path="departureDate" type="text" id="datePicker" />
				<form:errors path="departureDate" class="errors"></form:errors>
			</div>
			<div class="clear">
				<form:label path="departureHour">
					<spring:message code="flightrequest.departureHour"></spring:message>
				</form:label>
				<form:select path="departureHour">
					<form:option value=""></form:option>
					<form:options items="${hours}" />
				</form:select>
				<spring:message code="global.hour"></spring:message>
				<form:errors path="departureHour" class="errors"></form:errors>
				<div class="clear">
					<form:label path="departureMinute">
						<spring:message code="flightrequest.departureMinute"></spring:message>
					</form:label>
					<form:select path="departureMinute">
						<form:option value=""></form:option>
						<form:options items="${minutes}" />
					</form:select>
					<spring:message code="global.minute"></spring:message>
					<form:errors path="departureMinute" class="errors"></form:errors>
				</div>
			</div>
			<div class="clear">
				<button type="submit"><spring:message code="global.submit"></spring:message></button>
				 <a	href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"></spring:message></a>
			</div>
		</form:form>
	</div>
</body>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-1.9.1.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript">
	$('#datePicker').datepicker({
		dateFormat : 'yy-mm-dd',
		onSelect : function(dateText) {
			$('#datePicker').value(dateText);
		}
	});
</script>
</html>