<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet">
</head>
<body>
	<jsp:include page="header.jsp" />
	<div>
		<div>
			<h2>
				<img src="<%=request.getContextPath()%>/images/flightrequest.png">
				<spring:message code="flightrequest.header"></spring:message>
			</h2>
			<hr>
		</div>
		<a class="button" href="<%=request.getContextPath()%>/request.html"><spring:message
				code="flightrequest.create"></spring:message></a>
		<!-- Waiting Request table -->
		<div>
			<h3>
				<spring:message code="flightrequest.waiting"></spring:message>
			</h3>
			<table class="dataTable">
				<thead>
					<th><spring:message code="global.id"></spring:message></th>
					<th><spring:message code="flightrequest.status"></spring:message></th>
					<th><spring:message code="flightrequest.departureAirport"></spring:message></th>
					<th><spring:message code="flightrequest.arrivalAirport"></spring:message></th>
					<th><spring:message code="global.date"></spring:message></th>
					<th><spring:message code="global.customer"></spring:message></th>
					<th><spring:message code="flightrequest.noOfPassengers"></spring:message></th>
					<th><spring:message code="global.edit"></spring:message></th>
					<th>Confirm</th>
					<th>Reject</th>
					<th>Offer</th>
				</thead>
				<c:forEach items="${waitingRequests }" var="request">
					<tr>
						<td>${request.flightRequest.id}</td>
						<td>${request.flightRequest.statusNiceName}</td>
						<td>${request.departureAirport.name}
							(${request.departureAirport.code})</td>
						<td>${request.arrivalAirport.name}
							(${request.arrivalAirport.code})</td>
						<td>${request.flightRequest.niceDate}</td>
						<td>${request.flightRequest.customer.firstName}
							${request.flightRequest.customer.lastName }</td>
						<td>${request.flightRequest.numPassangers}</td>
						<td><a
							href="<%= request.getContextPath()%>/request.html?id=${request.flightRequest.id}"><img
								src="<%=request.getContextPath()%>/images/edit.png"></a></td>
						<c:choose>
							<c:when test="${request.flightRequest.status == 'CREATED' }">
								<td><a
									href="<%=request.getContextPath()%>/request.html?id=${request.flightRequest.id}&confirmed=1">Confirm</a></td>
								<td><a
									href="<%=request.getContextPath()%>/request.html?id=${request.flightRequest.id}&rejected=1">Reject</a></td>
								<td><a
									href="<%=request.getContextPath()%>/request.html?id=${request.flightRequest.id}&offer=1">Offer</a></td>
							</c:when>
							<c:otherwise>
								<td></td>
								<td></td>
								<td></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</table>
		</div>
		<!-- end Waiting Request table -->
		<!-- Request with offer table -->
		<div>
			<h3>
				<spring:message code="flightrequest.requestWithOffer"></spring:message>
			</h3>
			<table class="dataTable">
				<thead>
					<th><spring:message code="global.id"></spring:message></th>
					<th><spring:message code="flightrequest.status"></spring:message></th>
					<th><spring:message code="flightrequest.departureAirport"></spring:message></th>
					<th><spring:message code="flightrequest.arrivalAirport"></spring:message></th>
					<th><spring:message code="global.date"></spring:message></th>
					<th><spring:message code="global.customer"></spring:message></th>
					<th><spring:message code="flightrequest.noOfPassengers"></spring:message></th>
					<th><spring:message code="global.planeTypes"></spring:message></th>
					<th><spring:message code="flightrequest.offeredPrice"></spring:message></th>
				</thead>
				<c:forEach items="${requestsWithOffer }" var="request">
					<tr>
						<td>${request.flightRequest.id}</td>
						<td>${request.flightRequest.statusNiceName}</td>
						<td>${request.departureAirport.name}
							(${request.departureAirport.code})</td>
						<td>${request.arrivalAirport.name}
							(${request.arrivalAirport.code})</td>
						<td>${request.flightRequest.niceDate}</td>
						<td>${request.flightRequest.customer.firstName}
							${request.flightRequest.customer.lastName }</td>
						<td>${request.flightRequest.numPassangers}</td>
						<td>${request.flightRequest.offer.planeCode}</td>
						<td>${request.flightRequest.offer.offeredPrice} SEK</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<!-- end Request with offer table -->
		<!-- Rejected Request table -->
		<div>
			<h3>
				<spring:message code="flightrequest.rejectedRequest"></spring:message>
			</h3>
			<table class="dataTable">
				<thead>
					<th><spring:message code="global.id"></spring:message></th>
					<th><spring:message code="flightrequest.status"></spring:message></th>
					<th><spring:message code="flightrequest.departureAirport"></spring:message></th>
					<th><spring:message code="flightrequest.arrivalAirport"></spring:message></th>
					<th><spring:message code="global.date"></spring:message></th>
					<th><spring:message code="global.customer"></spring:message></th>
					<th><spring:message code="flightrequest.noOfPassengers"></spring:message></th>
				</thead>
				<c:forEach items="${rejectedRequests }" var="request">
					<tr>
						<td>${request.flightRequest.id}</td>
						<td>${request.flightRequest.statusNiceName}</td>
						<td>${request.departureAirport.name}
							(${request.departureAirport.code})</td>
						<td>${request.arrivalAirport.name}
							(${request.arrivalAirport.code})</td>
						<td>${request.flightRequest.niceDate}</td>
						<td>${request.flightRequest.customer.firstName}
							${request.flightRequest.customer.lastName }</td>
						<td>${request.flightRequest.numPassangers}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<!-- end Rejected Request table -->
	</div>
	<!-- Customer table -->
	<div>
		<div>
			<h2>
				<img src="<%=request.getContextPath()%>/images/customer.png">
				<spring:message code="global.customer"></spring:message>
			</h2>
			<hr>
		</div>
		<div class="margin-small">
			<a class="button" href="<%=request.getContextPath()%>/customer.html">
				<spring:message code="customer.create"></spring:message>
			</a>
		</div>
		<table class="dataTable">
			<thead>
				<th><spring:message code="global.id"></spring:message></th>
				<th><spring:message code="customer.firstName"></spring:message></th>
				<th><spring:message code="customer.lastName"></spring:message></th>
				<th><spring:message code="customer.company"></spring:message></th>
				<th><spring:message code="global.edit"></spring:message></th>
			</thead>
			<c:forEach items="${customers}" var="customer">
				<tr>
					<td>${customer.id}</td>
					<td>${customer.firstName}</td>
					<td>${customer.lastName}</td>
					<td>${customer.company}</td>
					<td><a
						href="<%= request.getContextPath() %>/customer.html?id=${customer.id}"><img
							src="<%=request.getContextPath()%>/images/edit.png"></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<!-- end Customer table -->
</body>

</html>