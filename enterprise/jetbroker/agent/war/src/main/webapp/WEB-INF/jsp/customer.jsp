<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Customer</title>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div>
		<div>
			<h1>
				<img src="<%=request.getContextPath()%>/images/customer.png">
				<spring:message code="customer.add"></spring:message>
			</h1>
		</div>
		<div id="formDiv">
			<form:form commandName="customer" action="/customer.html"
				method="POST">
				<form:input path="id" type="hidden" />
				<div>
					<form:label class="floatLeft" path="" for="firstName">
						<spring:message code="customer.firstName"></spring:message>
					</form:label>
					<form:input id="firstName" path="firstName"
						maxlength="25" />
					<form:errors path="firstName" class="errors"></form:errors>
				</div>
				<div class="clear">
					<form:label path="lastName">
						<spring:message code="customer.lastName"></spring:message>
					</form:label>
					<form:input path="lastName" maxlength="25" />
					<form:errors path="lastName" class="errors"></form:errors>
				</div>
				<div class="clear">
					<form:label path="">
						<spring:message code="global.email"></spring:message>
					</form:label>
					<form:input path="email" maxlength="25" />
					<form:errors path="email" class="errors"></form:errors>
				</div>
				<div class="clear">
					<form:label path="">
						<spring:message code="customer.company"></spring:message>
					</form:label>
					<form:input path="company" maxlength="25" />
					<form:errors path="company" class="errors"></form:errors>
				</div>
				<div class="clear">
					<button type="submit"><spring:message code="global.submit"></spring:message></button> 
					<a href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"></spring:message></a>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>