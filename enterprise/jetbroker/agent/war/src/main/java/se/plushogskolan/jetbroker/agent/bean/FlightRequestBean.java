package se.plushogskolan.jetbroker.agent.bean;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;
import se.plushogskolan.jee.utils.validation.ValidFlightRequest;

@ValidFlightRequest
public class FlightRequestBean implements ArrivalAndDepartureAirportHolder {

	private String id;
	@NotBlank(message = "{customer.notNull}")
	private String userId;
	@NotBlank
	private String departureAirport;
	@NotBlank
	private String arrivalAirport;
	@Range(min = 1, max = 500)
	private String numPassangers;
	@NotBlank
	@Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}")
	private String departureDate;
	@NotBlank
	private String departureHour;
	@NotBlank
	private String departureMinute;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirportCode) {
		this.departureAirport = departureAirportCode;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirportCode) {
		this.arrivalAirport = arrivalAirportCode;
	}

	public String getNumPassangers() {
		return numPassangers;
	}

	public void setNumPassangers(String numPassangers) {
		this.numPassangers = numPassangers;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureHour() {
		return departureHour;
	}

	public void setDepartureHour(String departureHour) {
		this.departureHour = departureHour;
	}

	public String getDepartureMinute() {
		return departureMinute;
	}

	public void setDepartureMinute(String departureMinute) {
		this.departureMinute = departureMinute;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getDepartureAirportCode() {
		return getDepartureAirport();
	}

	@Override
	public String getArrivalAirportCode() {
		return getArrivalAirport();
	}
}
