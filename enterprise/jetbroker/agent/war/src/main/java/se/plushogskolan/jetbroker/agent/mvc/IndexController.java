package se.plushogskolan.jetbroker.agent.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.bean.IndexBean;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

@Controller
public class IndexController {

	@Inject
	private CustomerService customerService;
	@Inject
	private FlightRequestService flightRequestService;

	@RequestMapping("/index.html")
	public ModelAndView index() {

		ModelAndView result = new ModelAndView("index");
		result.addObject("customers", customerService.getAllCustomers());

		List<IndexBean> beans = getIndexBeans(FlightRequestStatus.CREATED);
		result.addObject("waitingRequests", beans);

		beans = getIndexBeans(FlightRequestStatus.REJECTED);
		result.addObject("rejectedRequests", beans);

		beans = getIndexBeans(FlightRequestStatus.OFFER_RECIVED);
		result.addObject("requestsWithOffer", beans);

		return result;
	}

	private ArrayList<IndexBean> getIndexBeans(FlightRequestStatus status) {
		ArrayList<IndexBean> list = new ArrayList<IndexBean>();
		IndexBean bean;

		List<FlightRequest> requests;

		switch (status) {
		case CREATED:
			requests = flightRequestService.getAllWaitingRequests();
			break;

		case REJECTED:
			requests = flightRequestService.getAllRejectedRequests();
			break;

		case OFFER_RECIVED:
			requests = flightRequestService.getAllRequestsWithOffer();
			break;

		default:
			throw new IllegalArgumentException(status
					+ " is not a valid status");
		}

		for (FlightRequest request : requests) {

			bean = new IndexBean();
			bean.setFlightRequest(request);
			bean.setArrivalAirport(flightRequestService.getAirport(request
					.getArrivalAirport()));
			bean.setDepartureAirport(flightRequestService.getAirport(request
					.getDepartureAirport()));

			list.add(bean);
		}
		return list;
	}
}
