package se.plushogskolan.jetbroker.agent.mvc;

import javax.inject.Inject;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.bean.FlightRequestBean;
import se.plushogskolan.jetbroker.agent.domain.customer.Customer;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.event.FlightRequestEventStatus;
import se.plushogskolan.jetbroker.agent.integration.simulator.MdbSimulator;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;
import se.plushogskolan.jetbroker.agent.util.TimeUtil;

@Controller
@RequestMapping("/request.html")
public class FlightRequestController {

	private static final String REQUEST = "request";
	@Inject
	private CustomerService customerService;
	@Inject
	private FlightRequestService flightRequestService;
	@Inject
	private MdbSimulator mdbSimulator;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getRequest(FlightRequestBean request) {
		ModelAndView result = setKeys(request);

		return result;
	}

	@RequestMapping(method = RequestMethod.GET, params = { "id", "offer" })
	public ModelAndView getOffer(@RequestParam long id) {
		ModelAndView view = new ModelAndView("redirect:index.html");
		mdbSimulator.simulateOffer(id);
		return view;
	}

	@FlightRequestChanged(FlightRequestEventStatus.CONFIRMED)
	@RequestMapping(method = RequestMethod.GET, params = { "id", "confirmed" })
	public ModelAndView getConfirmedRequest(@RequestParam long id) {
		ModelAndView view = new ModelAndView("redirect:index.html");
		mdbSimulator.simulateConfirmation(id, 0);
		return view;
	}

	@FlightRequestChanged(FlightRequestEventStatus.REJECTED)
	@RequestMapping(method = RequestMethod.GET, params = { "id", "rejected" })
	public ModelAndView getRejectedRequest(@RequestParam long id) {
		ModelAndView view = new ModelAndView("redirect:index.html");
		mdbSimulator.simulateRejection(id, 0);
		return view;
	}

	@RequestMapping(method = RequestMethod.GET, params = { "id" })
	public ModelAndView editRequest(@RequestParam long id,
			FlightRequestBean bean) {

		FlightRequest flightRequest = flightRequestService.getFlightRequest(id);

		beanInit(bean, flightRequest);

		bean.setDepartureDate(TimeUtil.getDateString(flightRequest
				.getDepartureDate()));

		bean.setDepartureHour(TimeUtil.getHourString(flightRequest
				.getDepartureDate()));

		bean.setDepartureMinute(TimeUtil.getMinuteString(flightRequest
				.getDepartureDate()));

		ModelAndView result = setKeys(bean);
		result.addObject(REQUEST, bean);

		return result;
	}

	private void beanInit(FlightRequestBean request, FlightRequest flightRequest) {
		request.setId(String.valueOf(flightRequest.getId()));
		request.setUserId(String.valueOf(flightRequest.getCustomer().getId()));
		request.setDepartureAirport(flightRequest.getDepartureAirport());
		request.setArrivalAirport(flightRequest.getArrivalAirport());
		request.setNumPassangers(String.valueOf(flightRequest
				.getNumPassangers()));
	}

	private ModelAndView setKeys(FlightRequestBean bean) {
		ModelAndView result = new ModelAndView(REQUEST);

		result.addObject("customers", customerService.getAllCustomers());
		result.addObject("airports", flightRequestService.getAllAirports());
		result.addObject("hours", TimeUtil.getHours());
		result.addObject("minutes", TimeUtil.getMinutes());
		return result;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView postRequest(@Valid FlightRequestBean bean,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return setKeys(bean);
		}

		Customer foundCustomer = customerService.getCustomer(Long.valueOf(bean
				.getUserId()));

		DateTime dateTime = new DateTime(bean.getDepartureDate());

		dateTime = dateTime.hourOfDay().setCopy(bean.getDepartureHour())
				.minuteOfHour().setCopy(bean.getDepartureMinute());

		FlightRequest result;

		if (bean.getId().equals("")) {
			result = new FlightRequest(foundCustomer, Integer.valueOf(bean
					.getNumPassangers()), bean.getDepartureAirport(),
					bean.getArrivalAirport(), new DateTime(dateTime));

			flightRequestService.createFlightRequest(result);

		} else {

			result = flightRequestService.getFlightRequest(Long.valueOf(bean
					.getId()));

			result.setArrivalAirport(bean.getArrivalAirport());
			result.setCustomer(foundCustomer);
			result.setDepartureAirport(bean.getDepartureAirport());
			result.setDepartureDate(new DateTime(dateTime));
			result.setNumPassangers(Integer.valueOf(bean.getNumPassangers()));

			flightRequestService.updateFlightRequest(result);

		}

		return new ModelAndView("redirect:index.html");
	}
}
