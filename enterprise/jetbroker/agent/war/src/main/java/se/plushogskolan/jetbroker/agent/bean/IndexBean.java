package se.plushogskolan.jetbroker.agent.bean;

import se.plushogskolan.jetbroker.agent.domain.airport.Airport;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;

public class IndexBean {

	private Airport departureAirport;
	private Airport arrivalAirport;
	private FlightRequest flightRequest;

	public Airport getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public FlightRequest getFlightRequest() {
		return flightRequest;
	}

	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	}
}
