package se.plushogskolan.jetbroker.agent.mvc;

import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.customer.Customer;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@Controller
@RequestMapping("/customer.html")
public class CustomerController {

	@Autowired
	private MessageSource messageSource;
	@Inject
	private CustomerService customerService;
	private static Logger log = Logger.getLogger(CustomerController.class
			.toString());

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getCustomer(Customer customer) {

		return new ModelAndView("customer");
	}

	@RequestMapping(method = RequestMethod.GET, params = { "id" })
	public ModelAndView getCustomerById(@RequestParam long id, Customer customer) {
		Customer result = customerService.getCustomer(id);

		if (result == null) {
			throw new IllegalArgumentException(id + " was not found!");
		}

		ModelAndView model = new ModelAndView("customer");
		model.addObject("customer", result);

		return model;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView postCustomer(@Valid Customer customer,
			BindingResult bindingResult, Locale locale) {

		if (bindingResult.hasErrors()) {
			ModelAndView view = new ModelAndView("customer");

			view.addObject("customer", customer);
			return view;
		}

		if (customer.getFirstName().equals("i18n")) {
			log.info(messageSource
					.getMessage("i18n.test.message", null, locale));
		}

		if (customer.getId() == 0) {
			customerService.createCustomer(customer);

		} else {
			customerService.updateCustomer(customer);
		}
		return new ModelAndView("redirect:/index.html");
	}
}
