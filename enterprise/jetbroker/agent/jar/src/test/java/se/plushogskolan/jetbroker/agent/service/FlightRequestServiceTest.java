package se.plushogskolan.jetbroker.agent.service;

import javax.enterprise.event.Event;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.offer.Offer;
import se.plushogskolan.jetbroker.agent.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.integration.fascade.Fasade;
import se.plushogskolan.jetbroker.agent.repo.AirportRepository;
import se.plushogskolan.jetbroker.agent.repo.FlightRequestRepository;

public class FlightRequestServiceTest {

	private FlightRequestService flightRequestService;
	private Event<FlightRequestChangedEvent> emitter;
	private AirportRepository airportRepository;
	private FlightRequestRepository flightRequestRepository;
	private FlightRequest flightRequest;
	private Fasade fasade;

	@Before
	public void setUp() throws Exception {
		flightRequest = new FlightRequest();

		emitter = EasyMock.createMock(Event.class);
		airportRepository = EasyMock.createMock(AirportRepository.class);
		fasade = EasyMock.createMock(Fasade.class);

		flightRequestRepository = EasyMock
				.createMock(FlightRequestRepository.class);

		flightRequestService = new FlightRequestServiceImpl(emitter, emitter,
				emitter, flightRequestRepository, airportRepository, fasade);
	}

	@Test
	public void testFlightRequestUpdateEvents() {
		emitter.fire(EasyMock.isA(FlightRequestChangedEvent.class));
		EasyMock.expectLastCall().times(3);
		EasyMock.replay(emitter);

		flightRequestService.confirmFlightRequest(flightRequest, 0);
		flightRequestService.rejectFlightRequest(flightRequest, 0);
		flightRequestService.setFlightRequestOffer(flightRequest, new Offer());

		EasyMock.verify(emitter);
	}

	@Test
	public void testCreateFlightRequest() throws Exception {
		EasyMock.expect(flightRequestRepository.persist(flightRequest))
				.andReturn(new Long(0));

		EasyMock.replay(flightRequestRepository);

		flightRequestService.createFlightRequest(flightRequest);
		EasyMock.verify(flightRequestRepository);
	}

	@Test
	public void testDeleteFlightRequest() throws Exception {
		flightRequestRepository.remove(flightRequest);
		EasyMock.expectLastCall().once();

		flightRequestRepository.remove(flightRequest);
	}

	@Test
	public void testUpdateFlightRequest() throws Exception {
		flightRequestRepository.update(flightRequest);
		EasyMock.expectLastCall().once();

		flightRequestService.updateFlightRequest(flightRequest);
	}

	@Test
	public void testGetFlightRequestsWithOffer() throws Exception {
		EasyMock.expect(flightRequestRepository.getAllRequestsWithOffer())
				.andReturn(null);
		EasyMock.replay(flightRequestRepository);

		flightRequestService.getAllRequestsWithOffer();
		EasyMock.verify(flightRequestRepository);
	}

	@Test
	public void testGetRejectedFlightRequests() throws Exception {
		EasyMock.expect(flightRequestRepository.getAllRejectedRequests())
				.andReturn(null);
		EasyMock.replay(flightRequestRepository);

		flightRequestService.getAllRejectedRequests();
		EasyMock.verify(flightRequestRepository);
	}

	@Test
	public void testGetWaitingFlightRequests() throws Exception {
		EasyMock.expect(flightRequestRepository.getAllWaitingRequests())
				.andReturn(null);
		EasyMock.replay(flightRequestRepository);

		flightRequestService.getAllWaitingRequests();
		EasyMock.verify(flightRequestRepository);
	}
}
