package se.plushogskolan.jetbroker.agent.service;

import javax.enterprise.event.Event;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.integration.simulator.MdbSimulator;
import se.plushogskolan.jetbroker.agent.integration.simulator.MdbSimulatorImpl;

public class MdbSimulatorTest {

	private MdbSimulator simulator;
	private Event<FlightRequestChangedEvent> eventEmitter;

	@Before
	public void setUp() throws Exception {
		eventEmitter = EasyMock.createMock(Event.class);

		simulator = new MdbSimulatorImpl(eventEmitter, eventEmitter,
				eventEmitter);
	}

	@Test
	public void testEvents() {
		eventEmitter.fire(EasyMock.isA(FlightRequestChangedEvent.class));
		EasyMock.expectLastCall().times(3);
		EasyMock.replay(eventEmitter);

		simulator.simulateConfirmation(0, 0);
		simulator.simulateOffer(0);
		simulator.simulateRejection(0, 0);
		EasyMock.verify(eventEmitter);
	}
}
