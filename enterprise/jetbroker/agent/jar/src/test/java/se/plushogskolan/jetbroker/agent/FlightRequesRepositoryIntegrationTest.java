package se.plushogskolan.jetbroker.agent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.agent.domain.customer.Customer;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.repo.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.agent.repo.FlightRequestRepository;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.util.TestFixture;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class FlightRequesRepositoryIntegrationTest extends
		AbstractRepositoryTest<FlightRequest, FlightRequestRepository> {

	@Inject
	private FlightRequestRepository repo;

	@Inject
	private CustomerService customerService;

	private long id;

	@Override
	protected FlightRequestRepository getRepository() {
		return repo;
	}

	@Override
	protected FlightRequest getEntity1() {
		long id = customerService.createCustomer(TestFixture.getCustomer());

		return TestFixture.getFlightRequest(customerService.getCustomer(id));
	}

	@Override
	protected FlightRequest getEntity2() {
		long id = customerService.createCustomer(TestFixture.getCustomer());

		return TestFixture.getFlightRequest(customerService.getCustomer(id),
				"LHR", "GBG");
	}

	@Before
	public void setUp() {
		id = customerService.createCustomer(TestFixture.getCustomer());

		Customer customer = customerService.getCustomer(id);
		id = repo.persist(TestFixture.getFlightRequest(customer));
	}

	@Test
	public void testGetCustomer() throws Exception {
		FlightRequest flightRequest = repo.findById(id);

		assertNotNull("req not null", flightRequest);
		assertNotNull("customer not null", flightRequest.getCustomer());
	}

	@Test
	public void testGetDate() throws Exception {
		FlightRequest flightRequest = repo.findById(id);

		assertNotNull("req not null", flightRequest);
		assertNotNull("date not null", flightRequest.getDepartureDate());
	}

	@Test
	public void testGetStatus() throws Exception {
		FlightRequest flightRequest = repo.findById(id);

		assertNotNull("req not null", flightRequest);
		assertEquals("status created", FlightRequestStatus.CREATED,
				flightRequest.getStatus());
	}

	@Test(expected = Exception.class)
	public void testSaveRequest_invalid() throws Exception {
		FlightRequest flightRequest = new FlightRequest();
		repo.persist(flightRequest);
	}
}
