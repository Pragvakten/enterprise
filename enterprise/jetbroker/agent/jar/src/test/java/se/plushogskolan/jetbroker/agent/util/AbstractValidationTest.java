package se.plushogskolan.jetbroker.agent.util;

import static org.junit.Assert.fail;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Before;

public abstract class AbstractValidationTest<T> {

	protected Validator validator;

	@Before
	public void setUp() throws Exception {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	protected void assertPropertyIsInvalid(String property,
			Set<ConstraintViolation<T>> violations) {

		boolean errorFound = false;
		for (ConstraintViolation<T> constraintViolation : violations) {
			if (constraintViolation.getPropertyPath().toString()
					.equals(property)) {
				errorFound = true;
				break;
			}
		}

		if (!errorFound) {
			fail("Expected validation error for '" + property
					+ "', but no such error exists");
		}
	}
}
