package se.plushogskolan.jetbroker.agent.domain.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

import se.plushogskolan.jetbroker.agent.util.AbstractValidationTest;

public class CustomerTest extends AbstractValidationTest<Customer> {

	private static final String COMPANY = "baz";
	private static final String EMAIL = "foo@bar.se";
	private static final String LASTNAME = "bar";
	private static final String FIRSTNAME = "foo";
	private Customer customer;
	private Customer customer2;
	private Customer customer3;

	@Test
	public void testCustomer() throws Exception {
		customer = new Customer(FIRSTNAME, LASTNAME, EMAIL, COMPANY);

		assertEquals("get id 0", 0, customer.getId());
		assertEquals("name correct", FIRSTNAME, customer.getFirstName());
		assertEquals("lastname correct", LASTNAME, customer.getLastName());
		assertEquals("email correct", EMAIL, customer.getEmail());
		assertEquals("company correct", COMPANY, customer.getCompany());
	}

	@Test
	public void testCustomer2() throws Exception {
		customer2 = new Customer(FIRSTNAME, LASTNAME);

		assertEquals("get id 0", 0, customer2.getId());
		assertEquals("name correct", FIRSTNAME, customer2.getFirstName());
		assertEquals("lastname correct", LASTNAME, customer2.getLastName());
		assertNull("email null", customer2.getEmail());
		assertNull("company null", customer2.getCompany());

	}

	@Test
	public void testCustomer3() throws Exception {
		customer3 = new Customer();

		assertEquals("get id 0", 0, customer3.getId());
		assertNull("name null", customer3.getFirstName());
		assertNull("lastname null", customer3.getLastName());
		assertNull("email null", customer3.getEmail());
		assertNull("company null", customer3.getCompany());

	}

	@Test
	public void testCustomerFirstName_Null() throws Exception {
		customer = new Customer();
		Set<ConstraintViolation<Customer>> validate = validator
				.validate(customer);

		assertPropertyIsInvalid("firstName", validate);
	}

	@Test
	public void testCustomerLastName_Null() throws Exception {
		customer = new Customer();
		Set<ConstraintViolation<Customer>> validate = validator
				.validate(customer);

		assertPropertyIsInvalid("lastName", validate);
	}

	@Test(expected = AssertionError.class)
	public void testCustomerEmail() throws Exception {
		customer = new Customer();
		customer.setEmail("foo@bar.se");
		Set<ConstraintViolation<Customer>> validate = validator
				.validate(customer);

		assertPropertyIsInvalid("email", validate);
	}

	@Test
	public void testCustomerEmail_invalid() throws Exception {
		customer = new Customer();
		customer.setEmail("asd");
		Set<ConstraintViolation<Customer>> validate = validator
				.validate(customer);

		assertPropertyIsInvalid("email", validate);
	}

}
