package se.plushogskolan.jetbroker.agent.util;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.agent.domain.customer.Customer;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.toString());

	public static Customer getCustomer() {
		return new Customer("foo", "bar", "foo@bar.se", "baz");
	}

	public static Customer getCustomer(String firstName, String lastName) {
		Customer customer = getCustomer();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);

		return customer;
	}

	public static Archive<?> createIntegrationTestArchive() {
		MavenDependencyResolver mvnResolver = DependencyResolvers.use(
				MavenDependencyResolver.class).loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "agent_test.war")
				.addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml")
				.addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2")
				.resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2")
				.resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact(
				"org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}

	public static FlightRequest getFlightRequest(Customer customer) {
		return new FlightRequest(customer, 1, "GBG", "LHR", new DateTime());
	}

	public static FlightRequest getFlightRequest(Customer customer,
			String departure, String arrival) {

		FlightRequest request = getFlightRequest(customer);
		request.setDepartureAirport(departure);
		request.setArrivalAirport(arrival);

		return request;
	}
}
