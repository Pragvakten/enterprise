package se.plushogskolan.jetbroker.agent;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.agent.domain.customer.Customer;
import se.plushogskolan.jetbroker.agent.repo.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.agent.repo.CustomerRepository;
import se.plushogskolan.jetbroker.agent.util.TestFixture;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class CustomerRepositoryIntegrationTest extends
		AbstractRepositoryTest<Customer, CustomerRepository> {

	@Inject
	CustomerRepository repo;

	@Test
	public void testGetAllCustomers() throws Exception {
		List<Customer> allCustomers = repo.getAllCustomers();
		assertEquals("list length 0", 0, allCustomers.size());
	}

	@Override
	protected CustomerRepository getRepository() {
		return repo;
	}

	@Override
	protected Customer getEntity1() {
		return TestFixture.getCustomer();
	}

	@Override
	protected Customer getEntity2() {
		return TestFixture.getCustomer("bar", "baz");
	}

	@Test(expected = Exception.class)
	public void testSaveCustomer_invalid() throws Exception {
		Customer customer = new Customer();
		repo.persist(customer);
	}

}
