package se.plushogskolan.jetbroker.agent.domain.flightrequest;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.util.AbstractValidationTest;

public class FlightRequestTest extends AbstractValidationTest<FlightRequest> {

	private FlightRequest request;

	@Before
	public void setUp() throws Exception {
		request = new FlightRequest();
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	@Test
	public void testGetStatus() throws Exception {
		assertEquals("status is created", FlightRequestStatus.CREATED,
				request.getStatus());
	}

	@Test
	public void testCustomer_null() throws Exception {
		Set<ConstraintViolation<FlightRequest>> validate = validator
				.validate(request);

		assertPropertyIsInvalid("customer", validate);
	}

	@Test(expected = AssertionError.class)
	public void testNumPassangers_1() throws Exception {
		request.setNumPassangers(1);
		Set<ConstraintViolation<FlightRequest>> validate = validator
				.validate(request);

		assertPropertyIsInvalid("numPassangers", validate);
	}

	@Test(expected = AssertionError.class)
	public void testNumPassangers_500() throws Exception {
		request.setNumPassangers(500);
		Set<ConstraintViolation<FlightRequest>> validate = validator
				.validate(request);

		assertPropertyIsInvalid("numPassangers", validate);
	}

	@Test
	public void testNumPassangers_0() throws Exception {
		Set<ConstraintViolation<FlightRequest>> validate = validator
				.validate(request);

		assertPropertyIsInvalid("numPassangers", validate);
	}

	@Test
	public void testNumPassangers_501() throws Exception {
		request.setNumPassangers(501);
		Set<ConstraintViolation<FlightRequest>> validate = validator
				.validate(request);

		assertPropertyIsInvalid("numPassangers", validate);
	}

	@Test
	public void testValidAirports() throws Exception {
		request.setArrivalAirport("GBG");
		request.setDepartureAirport("GBG");

		Set<ConstraintViolation<FlightRequest>> validate = validator
				.validate(request);
		assertPropertyIsInvalid("", validate);
	}
}
