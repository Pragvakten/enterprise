package se.plushogskolan.jetbroker.agent.integration.simulator;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.validation.Mock;
import se.plushogskolan.jetbroker.agent.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.domain.offer.Offer;
import se.plushogskolan.jetbroker.agent.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.event.FlightRequestEventStatus;

/**
 * Temp simulator for mocking external messages
 * 
 * @author fidde
 * 
 */
@Stateless
public class MdbSimulatorImpl implements MdbSimulator {

	@Inject
	@Mock
	@FlightRequestChanged(FlightRequestEventStatus.CONFIRMED)
	private Event<FlightRequestChangedEvent> confirmedEvent;

	@Inject
	@Mock
	@FlightRequestChanged(FlightRequestEventStatus.REJECTED)
	private Event<FlightRequestChangedEvent> rejectedEvent;

	@Inject
	@Mock
	@FlightRequestChanged(FlightRequestEventStatus.OFFER_RECEIVED)
	private Event<FlightRequestChangedEvent> offerEvent;

	public MdbSimulatorImpl(Event<FlightRequestChangedEvent> confirmedEvent,
			Event<FlightRequestChangedEvent> offerEvent,
			Event<FlightRequestChangedEvent> rejectedEvent) {

		this.confirmedEvent = confirmedEvent;
		this.offerEvent = offerEvent;
		this.rejectedEvent = rejectedEvent;
	}

	@Override
	public void simulateOffer(long id) {
		// mock offer
		Offer offer = new Offer("1 000", "Airbus 1337 (250 seats)");
		offerEvent.fire(new FlightRequestChangedEvent(id, offer));
	}

	@Override
	public void simulateConfirmation(long id, long confirmId) {
		confirmedEvent.fire(new FlightRequestChangedEvent(id, confirmId));
	}

	@Override
	public void simulateRejection(long id, long rejectId) {
		rejectedEvent.fire(new FlightRequestChangedEvent(id, rejectId));
	}
}
