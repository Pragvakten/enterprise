package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.customer.Customer;

@Local
public interface CustomerService {

	Customer getCustomer(long id);

	void deleteCustomer(long id);

	void updateCustomer(Customer customer);

	long createCustomer(Customer customer);

	List<Customer> getAllCustomers();
}
