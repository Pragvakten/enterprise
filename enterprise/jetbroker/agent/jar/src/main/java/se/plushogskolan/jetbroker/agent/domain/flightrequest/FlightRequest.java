package se.plushogskolan.jetbroker.agent.domain.flightrequest;

import java.text.SimpleDateFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;
import se.plushogskolan.jee.utils.validation.ValidFlightRequest;
import se.plushogskolan.jetbroker.agent.domain.customer.Customer;
import se.plushogskolan.jetbroker.agent.domain.offer.Offer;

/**
 * Entity representing a request for a flight.
 * 
 * @author fidde
 * 
 */
@Entity
@ValidFlightRequest
public class FlightRequest implements IdHolder,
		ArrivalAndDepartureAirportHolder {

	@Id
	@GeneratedValue
	private long id;
	@OneToOne
	@NotNull(message = "{customer.notNull}")
	private Customer customer;
	@Range(min = 1, max = 500)
	private int numPassangers;
	@NotBlank
	private String departureAirport;
	@NotBlank
	private String arrivalAirport;
	@Column
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime departureDate;
	@NotNull
	private FlightRequestStatus status;
	@OneToOne(cascade = CascadeType.ALL)
	private Offer offer;

	public FlightRequest() {
		this(null, 0, null, null, null);
	}

	public FlightRequest(Customer customer, int numPassangers,
			String departureAirport, String arrivalAirport, DateTime date) {

		setCustomer(customer);
		setNumPassangers(numPassangers);
		setDepartureAirport(departureAirport);
		setArrivalAirport(arrivalAirport);
		setDepartureDate(date);
		setStatus(FlightRequestStatus.CREATED);
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getNumPassangers() {
		return numPassangers;
	}

	public void setNumPassangers(int numPassangers) {
		this.numPassangers = numPassangers;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public DateTime getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(DateTime date) {
		this.departureDate = date;
	}

	public FlightRequestStatus getStatus() {
		return status;
	}

	public void setStatus(FlightRequestStatus status) {
		this.status = status;
	}

	public String getNiceDate() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(departureDate
				.toDate());
	}

	public String getStatusNiceName() {
		return status.toString();
	}

	@Override
	public String toString() {
		return "FlightRequest [id=" + id + ", customer=" + customer
				+ ", numPassangers=" + numPassangers + ", departureAirport="
				+ departureAirport + ", arrivalAirport=" + arrivalAirport
				+ ", date=" + departureDate + ", status=" + status + "]";
	}

	@Override
	public String getDepartureAirportCode() {
		return departureAirport;
	}

	@Override
	public String getArrivalAirportCode() {
		return arrivalAirport;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

}
