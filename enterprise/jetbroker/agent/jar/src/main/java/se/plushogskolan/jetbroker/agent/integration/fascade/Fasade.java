package se.plushogskolan.jetbroker.agent.integration.fascade;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;

@Local
public interface Fasade {

	void sendFlightRequest(FlightRequest flightRequest);
}
