package se.plushogskolan.jetbroker.agent.domain.customer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Entity representing a customer.
 * 
 * @author fidde
 * 
 */
@Entity
public class Customer implements IdHolder {

	@Id
	@GeneratedValue
	private long id;
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@NotBlank
	@Email
	private String email;
	@NotBlank
	private String company;

	public Customer() {
		this(null, null);
	}

	public Customer(String firstname, String lastname) {
		this(firstname, lastname, null, null);
	}

	public Customer(String firstname, String lastname, String email,
			String company) {
		setFirstName(firstname);
		setLastName(lastname);
		setEmail(email);
		setCompany(company);
	}

	@Override
	public long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", company="
				+ company + "]";
	}

}
