package se.plushogskolan.jetbroker.agent.integration.mdb;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.validation.Mock;
import se.plushogskolan.jetbroker.agent.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.event.FlightRequestEventStatus;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Handles confirmation updates on flightrequests
 * 
 * @author fidde
 * 
 */
public class MdbConfirm {

	@Inject
	private FlightRequestService flightRequestService;

	public void getConfirm(
			@Observes @Mock @FlightRequestChanged(FlightRequestEventStatus.CONFIRMED) FlightRequestChangedEvent event) {

		FlightRequest flightRequest = flightRequestService
				.getFlightRequest(event.getFlightRequestID());
		flightRequestService.confirmFlightRequest(flightRequest, 0);
	}

}
