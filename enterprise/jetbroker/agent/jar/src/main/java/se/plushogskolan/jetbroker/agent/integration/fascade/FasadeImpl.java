package se.plushogskolan.jetbroker.agent.integration.fascade;

import java.util.logging.Logger;

import javax.ejb.Stateless;

import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;

@Stateless
public class FasadeImpl implements Fasade {

	private static Logger log = Logger
			.getLogger(FasadeImpl.class.toString());

	@Override
	public void sendFlightRequest(FlightRequest flightRequest) {
		log.info("FlightRequest sent");
	}

}
