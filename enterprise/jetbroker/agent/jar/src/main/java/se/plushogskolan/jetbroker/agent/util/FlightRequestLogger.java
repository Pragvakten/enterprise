package se.plushogskolan.jetbroker.agent.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import se.plushogskolan.jee.utils.validation.Prod;
import se.plushogskolan.jetbroker.agent.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.event.FlightRequestEventStatus;

@ApplicationScoped
public class FlightRequestLogger {

	private static int numConfirms;
	private static int numOffers;
	private static int numReject;

	public FlightRequestLogger() {

	}

	public void logOffer(
			@Observes @Prod @FlightRequestChanged(FlightRequestEventStatus.OFFER_RECEIVED) FlightRequestChangedEvent event) {
		numOffers++;
		log();
	}

	private void log() {
		System.out.println("***FlightRequestLogger***");
		System.out.println("numOffers: " + numOffers);
		System.out.println("numConfirms: " + numConfirms);
		System.out.println("numRejects: " + numReject);
		System.out.println("*********************************");
	}

	public void logConfirm(
			@Observes @Prod @FlightRequestChanged(FlightRequestEventStatus.CONFIRMED) FlightRequestChangedEvent event) {
		numConfirms++;
		log();
	}

	public void logReject(
			@Observes @Prod @FlightRequestChanged(FlightRequestEventStatus.REJECTED) FlightRequestChangedEvent event) {
		numReject++;
		log();
	}
}
