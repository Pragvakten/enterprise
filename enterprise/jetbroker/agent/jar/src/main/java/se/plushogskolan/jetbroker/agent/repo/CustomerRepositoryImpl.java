package se.plushogskolan.jetbroker.agent.repo;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.plushogskolan.jetbroker.agent.domain.customer.Customer;

public class CustomerRepositoryImpl implements CustomerRepository {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public long persist(Customer entity) {
		manager.persist(entity);
		manager.flush();
		
		return entity.getId();
	}

	@Override
	public void remove(Customer entity) {
		manager.remove(entity);
	}

	@Override
	public Customer findById(long id) {
		return manager.find(Customer.class, id);
	}

	@Override
	public void update(Customer entity) {
		manager.merge(entity);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return manager.createQuery("SELECT c FROM Customer c", Customer.class).getResultList();
	}
}
