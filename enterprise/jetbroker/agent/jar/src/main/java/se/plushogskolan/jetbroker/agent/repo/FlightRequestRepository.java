package se.plushogskolan.jetbroker.agent.repo;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;

public interface FlightRequestRepository extends BaseRepository<FlightRequest> {

	List<FlightRequest> getAllWaitingRequests();

	List<FlightRequest> getAllRejectedRequests();

	List<FlightRequest> getAllRequestsWithOffer();

}
