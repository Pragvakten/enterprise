package se.plushogskolan.jetbroker.agent.repo;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.airport.Airport;

public class AirportRepositoryImpl extends
		AbstractInMemoryRepostitory<String, Airport> implements
		AirportRepository {

	public AirportRepositoryImpl() {
		super();
		repo.put("GBG", new Airport("GBG", "Gothenburg", 23, 255));
		repo.put("LHR", new Airport("LHR", "London", 34, 555));
	}

	@Override
	public Airport getAirport(String code) {
		return repo.get(code);
	}

	@Override
	public List<Airport> getAllAirports() {
		return new ArrayList<Airport>(repo.values());
	}
}
