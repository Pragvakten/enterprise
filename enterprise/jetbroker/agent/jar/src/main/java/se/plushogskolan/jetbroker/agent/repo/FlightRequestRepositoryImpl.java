package se.plushogskolan.jetbroker.agent.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;

public class FlightRequestRepositoryImpl implements FlightRequestRepository {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public long persist(FlightRequest entity) {
		manager.persist(entity);
		manager.flush();

		return entity.getId();
	}

	@Override
	public void remove(FlightRequest entity) {
		manager.remove(entity);
	}

	@Override
	public FlightRequest findById(long id) {
		return manager.find(FlightRequest.class, id);
	}

	@Override
	public void update(FlightRequest entity) {
		manager.merge(entity);
	}

	@Override
	public List<FlightRequest> getAllWaitingRequests() {
		return manager.createQuery(
				"SELECT r FROM FlightRequest r WHERE status = 0 OR status = 1",
				FlightRequest.class).getResultList();
	}

	@Override
	public List<FlightRequest> getAllRejectedRequests() {
		return manager.createQuery(
				"SELECT r FROM FlightRequest r WHERE status = 3",
				FlightRequest.class).getResultList();

	}

	@Override
	public List<FlightRequest> getAllRequestsWithOffer() {
		return manager.createQuery(
				"SELECT r FROM FlightRequest r WHERE offer_id IS NOT null ",
				FlightRequest.class).getResultList();

	}
}
