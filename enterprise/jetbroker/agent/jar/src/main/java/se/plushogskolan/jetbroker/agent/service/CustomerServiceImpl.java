package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.customer.Customer;
import se.plushogskolan.jetbroker.agent.repo.CustomerRepository;

@Stateless
public class CustomerServiceImpl implements CustomerService{

	@Inject
	private CustomerRepository repo;
	
	@Override
	public Customer getCustomer(long id) {
		return repo.findById(id);
	}

	@Override
	public void deleteCustomer(long id) {
		repo.remove(getCustomer(id));
	}

	@Override
	public void updateCustomer(Customer customer) {
		repo.update(customer);
	}

	@Override
	public long createCustomer(Customer customer) {
		return repo.persist(customer);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return repo.getAllCustomers();
	}

}
