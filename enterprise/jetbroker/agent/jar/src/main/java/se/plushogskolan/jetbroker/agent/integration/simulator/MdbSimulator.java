package se.plushogskolan.jetbroker.agent.integration.simulator;

public interface MdbSimulator {

	public void simulateOffer(long flightRequestId);

	public void simulateConfirmation(long id, long confirmId);

	public void simulateRejection(long id, long rejectId);

}
