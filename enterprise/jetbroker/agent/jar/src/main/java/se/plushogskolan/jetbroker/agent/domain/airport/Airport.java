package se.plushogskolan.jetbroker.agent.domain.airport;

/**
 * Entity provided by the plane module.
 * 
 * @author fidde
 * 
 */
public class Airport implements Comparable<Airport> {

	private String code;
	private String name;
	private double latitude;
	private double longitude;

	public Airport() {

	}

	public Airport(String code, String name, double lat, double lng) {
		setCode(code);
		setName(name);
		setLatitude(lat);
		setLongitude(lng);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getNiceName() {
		return getCode() + " " + getName();
	}

	@Override
	public int compareTo(Airport o) {
		return getName().compareToIgnoreCase(o.getName());
	}

	@Override
	public String toString() {
		return "Airport [code=" + code + ", name=" + name + ", latitude="
				+ latitude + ", longitude=" + longitude + "]";
	}

}
