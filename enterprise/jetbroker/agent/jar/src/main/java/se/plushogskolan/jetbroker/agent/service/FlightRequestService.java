package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.airport.Airport;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.offer.Offer;

public interface FlightRequestService {

	FlightRequest getFlightRequest(long id);

	void deleteFlightRequest(FlightRequest request);

	void updateFlightRequest(FlightRequest request);

	long createFlightRequest(FlightRequest request);

	List<Airport> getAllAirports();

	Airport getAirport(String arrivalAirport);

	List<FlightRequest> getAllWaitingRequests();

	List<FlightRequest> getAllRejectedRequests();

	List<FlightRequest> getAllRequestsWithOffer();

	void rejectFlightRequest(FlightRequest flightRequest, long rejectId);

	void setFlightRequestOffer(FlightRequest flightRequest, Offer offer);

	void confirmFlightRequest(FlightRequest flightRequest, long confirmId);
}
