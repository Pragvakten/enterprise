package se.plushogskolan.jetbroker.agent.repo;


import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.customer.Customer;

public interface CustomerRepository extends BaseRepository<Customer>{

	List<Customer> getAllCustomers();
}
