package se.plushogskolan.jetbroker.agent.event;

import se.plushogskolan.jetbroker.agent.domain.offer.Offer;

public class FlightRequestChangedEvent {

	private long flightRequestID;
	private long orderId;
	private Offer offer;

	public FlightRequestChangedEvent() {

	}

	public FlightRequestChangedEvent(long flightRequestId, Offer offer) {
		setFlightRequestID(flightRequestId);
		setOffer(offer);
	}

	public FlightRequestChangedEvent(long flightRequestId, long orderID) {
		setFlightRequestID(flightRequestId);
		setOrderId(orderID);
	}

	public long getFlightRequestID() {
		return flightRequestID;
	}

	public void setFlightRequestID(long flightRequestID) {
		this.flightRequestID = flightRequestID;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

}
