package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.validation.Prod;
import se.plushogskolan.jetbroker.agent.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.domain.airport.Airport;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.domain.offer.Offer;
import se.plushogskolan.jetbroker.agent.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.event.FlightRequestEventStatus;
import se.plushogskolan.jetbroker.agent.integration.fascade.Fasade;
import se.plushogskolan.jetbroker.agent.repo.AirportRepository;
import se.plushogskolan.jetbroker.agent.repo.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	private Fasade fasade;
	@Inject
	@Prod
	@FlightRequestChanged(FlightRequestEventStatus.CONFIRMED)
	private Event<FlightRequestChangedEvent> confirmEmitter;
	@Inject
	@Prod
	@FlightRequestChanged(FlightRequestEventStatus.REJECTED)
	private Event<FlightRequestChangedEvent> rejectEmitter;
	@Inject
	@Prod
	@FlightRequestChanged(FlightRequestEventStatus.OFFER_RECEIVED)
	private Event<FlightRequestChangedEvent> offerEmitter;
	@Inject
	private FlightRequestRepository flightRequestRepo;
	@Inject
	private AirportRepository airportRepository;

	public FlightRequestServiceImpl() {

	}

	public FlightRequestServiceImpl(
			Event<FlightRequestChangedEvent> offerEmitter,
			Event<FlightRequestChangedEvent> confirmEmitter,
			Event<FlightRequestChangedEvent> rejectEmitter,
			FlightRequestRepository flightRequestRepo,
			AirportRepository airportRepository, Fasade fasade) {

		this.offerEmitter = offerEmitter;
		this.confirmEmitter = confirmEmitter;
		this.rejectEmitter = rejectEmitter;
		this.airportRepository = airportRepository;
		this.flightRequestRepo = flightRequestRepo;
		this.fasade = fasade;
	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return flightRequestRepo.findById(id);
	}

	@Override
	public void deleteFlightRequest(FlightRequest request) {
		flightRequestRepo.remove(request);
	}

	@Override
	public void updateFlightRequest(FlightRequest request) {
		flightRequestRepo.update(request);
		fasade.sendFlightRequest(request);
	}

	@Override
	public long createFlightRequest(FlightRequest request) {
		return flightRequestRepo.persist(request);
	}

	@Override
	public List<Airport> getAllAirports() {
		return airportRepository.getAllAirports();
	}

	@Override
	public List<FlightRequest> getAllWaitingRequests() {
		return flightRequestRepo.getAllWaitingRequests();
	}

	@Override
	public Airport getAirport(String arrivalAirport) {
		return airportRepository.getAirport(arrivalAirport);
	}

	@Override
	public List<FlightRequest> getAllRejectedRequests() {
		return flightRequestRepo.getAllRejectedRequests();
	}

	@Override
	public List<FlightRequest> getAllRequestsWithOffer() {
		return flightRequestRepo.getAllRequestsWithOffer();
	}

	@Override
	public void rejectFlightRequest(FlightRequest flightRequest, long rejectId) {
		flightRequest.setStatus(FlightRequestStatus.REJECTED);
		updateFlightRequest(flightRequest);

		rejectEmitter.fire(new FlightRequestChangedEvent());
	}

	@Override
	public void setFlightRequestOffer(FlightRequest flightRequest, Offer offer) {

		flightRequest.setOffer(offer);
		flightRequest.setStatus(FlightRequestStatus.OFFER_RECIVED);
		updateFlightRequest(flightRequest);

		offerEmitter.fire(new FlightRequestChangedEvent());
	}

	@Override
	public void confirmFlightRequest(FlightRequest flightRequest, long confirmId) {
		flightRequest.setStatus(FlightRequestStatus.REQUEST_CONFIRMED);
		updateFlightRequest(flightRequest);
		confirmEmitter.fire(new FlightRequestChangedEvent());

	}

}
