package se.plushogskolan.jetbroker.agent.event;

/**
 * Rejection object sent back to the agent module upon recived flightrequest
 * 
 * @author fidde
 * 
 */
public class FlightRequestRejection extends FlightRequestConfirmation {

	public FlightRequestRejection(long agentID, long confirmationID) {
		super(agentID, confirmationID);
	}

}
