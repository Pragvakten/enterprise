package se.plushogskolan.jetbroker.agent.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

public class TimeUtil {

	public static List<String> getHours() {
		ArrayList<String> result = new ArrayList<String>();

		for (int i = 0; i < 24; i++) {
			result.add(String.format("%02d", i));
		}

		return result;
	}

	public static List<String> getMinutes() {
		ArrayList<String> result = new ArrayList<String>();

		for (int i = 0; i < 60; i++) {
			result.add(String.format("%02d", i));
		}

		return result;
	}

	public static String getDateString(DateTime departureDate) {
		return new SimpleDateFormat("yyyy-MM-dd")
				.format(departureDate.toDate());
	}

	public static String getHourString(DateTime departureDate) {
		return String.format("%02d", departureDate.getHourOfDay());
	}

	public static String getMinuteString(DateTime departureDate) {
		return String.format("%02d", departureDate.getMinuteOfHour());
	}

}
