package se.plushogskolan.jetbroker.agent.repo;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.airport.Airport;

public interface AirportRepository {

	Airport getAirport(String code);

	List<Airport> getAllAirports();
}
