package se.plushogskolan.jetbroker.agent.event;

public enum FlightRequestEventStatus {

	CONFIRMED, OFFER_RECEIVED, REJECTED;
}
