package se.plushogskolan.jetbroker.agent.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

import se.plushogskolan.jetbroker.agent.event.FlightRequestEventStatus;

@Retention(RUNTIME)
@Target({ TYPE, METHOD, FIELD, PARAMETER })
@Qualifier
@Documented
public @interface FlightRequestChanged {

	FlightRequestEventStatus value();
}
