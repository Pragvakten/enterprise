package se.plushogskolan.jetbroker.agent.domain.offer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Entity containing data relevant to an offer made as a response to a
 * flightrequest from the agent module.
 * 
 * @author fidde
 * 
 */
@Entity
public class Offer {

	@Id
	@GeneratedValue
	private long id;
	private long agentID;
	private String offeredPrice;
	private String planeCode;

	public Offer() {

	}

	public Offer(String offeredPrice, String planeCode) {
		setOfferedPrice(offeredPrice);
		setPlaneCode(planeCode);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(String offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public String getPlaneCode() {
		return planeCode;
	}

	public void setPlaneCode(String planeId) {
		this.planeCode = planeId;
	}

	public long getAgentID() {
		return agentID;
	}

	public void setAgentID(long agentID) {
		this.agentID = agentID;
	}
}
