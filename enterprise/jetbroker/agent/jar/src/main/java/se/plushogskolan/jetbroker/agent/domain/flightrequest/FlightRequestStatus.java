package se.plushogskolan.jetbroker.agent.domain.flightrequest;

/**
 * The different statuses a flight request can be in.
 * 
 * @author fidde
 * 
 */
public enum FlightRequestStatus {
	CREATED("Created"), REQUEST_CONFIRMED("Request confirmed"), OFFER_RECIVED(
			"Offer recived"), REJECTED("Rejected");

	private final String niceName;

	private FlightRequestStatus(String niceName) {
		this.niceName = niceName;
	}

	@Override
	public String toString() {
		return niceName;
	}
}
