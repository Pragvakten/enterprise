package se.plushogskolan.jetbroker.agent.integration.mdb;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.validation.Mock;
import se.plushogskolan.jetbroker.agent.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.agent.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.event.FlightRequestEventStatus;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Handles rejection updates for flightrequests
 * 
 * @author fidde
 * 
 */
public class MdbReject {

	@Inject
	private FlightRequestService flightRequestService;

	public void getReject(
			@Observes @Mock @FlightRequestChanged(FlightRequestEventStatus.REJECTED) FlightRequestChangedEvent event) {

		FlightRequest flightRequest = flightRequestService
				.getFlightRequest(event.getFlightRequestID());
		flightRequestService.rejectFlightRequest(flightRequest, 0);

	}

}
