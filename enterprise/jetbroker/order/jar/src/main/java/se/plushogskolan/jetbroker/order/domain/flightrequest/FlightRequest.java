package se.plushogskolan.jetbroker.order.domain.flightrequest;

import java.text.SimpleDateFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;
import se.plushogskolan.jetbroker.order.domain.offer.Offer;

/**
 * Entity representing a request for a flight.
 * 
 * @author fidde
 * 
 */
@Entity
public class FlightRequest implements IdHolder,
		ArrivalAndDepartureAirportHolder {

	@Id
	@GeneratedValue
	private long id;
	private long agentID;
	private int numPassangers;
	private String departureAirport;
	private String arrivalAirport;
	@Column
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime departureDate;
	private FlightRequestStatus status;
	@OneToOne(cascade = CascadeType.ALL)
	private Offer offer;

	public FlightRequest() {
		this(0, null, null, null);
	}

	public FlightRequest(int numPassangers, String departureAirport,
			String arrivalAirport, DateTime date) {

		setNumPassangers(numPassangers);
		setDepartureAirport(departureAirport);
		setArrivalAirport(arrivalAirport);
		setDepartureDate(date);
		setStatus(FlightRequestStatus.NEW);
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNumPassangers() {
		return numPassangers;
	}

	public void setNumPassangers(int numPassangers) {
		this.numPassangers = numPassangers;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public DateTime getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(DateTime date) {
		this.departureDate = date;
	}

	public FlightRequestStatus getStatus() {
		return status;
	}

	public void setStatus(FlightRequestStatus status) {
		this.status = status;
	}

	public String getNiceDate() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(departureDate
				.toDate());
	}

	public String getStatusNiceName() {
		return status.toString();
	}

	@Override
	public String getDepartureAirportCode() {
		return departureAirport;
	}

	@Override
	public String getArrivalAirportCode() {
		return arrivalAirport;
	}

	@Override
	public String toString() {
		return "FlightRequest [id=" + id + ", numPassangers=" + numPassangers
				+ ", departureAirport=" + departureAirport
				+ ", arrivalAirport=" + arrivalAirport + ", departureDate="
				+ departureDate + ", status=" + status + "]";
	}

	public long getAgentID() {
		return agentID;
	}

	public void setAgentID(long agentID) {
		this.agentID = agentID;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

}
