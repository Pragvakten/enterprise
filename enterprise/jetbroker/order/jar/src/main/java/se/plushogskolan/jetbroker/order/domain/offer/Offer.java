package se.plushogskolan.jetbroker.order.domain.offer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jetbroker.order.annotation.ValidPlaneId;

/**
 * Entity containing data relevant to an offer made as a response to a
 * flightrequest from the agent module.
 * 
 * @author fidde
 * 
 */
@Entity
public class Offer {

	@Id
	@GeneratedValue
	private long id;
	@NotBlank
	private String offeredPrice;
	@ValidPlaneId
	private long planeId;

	public Offer() {

	}

	public Offer(String offeredPrice, long planeId) {
		setOfferedPrice(offeredPrice);
		setPlaneId(planeId);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(String offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public long getPlaneId() {
		return planeId;
	}

	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}
}
