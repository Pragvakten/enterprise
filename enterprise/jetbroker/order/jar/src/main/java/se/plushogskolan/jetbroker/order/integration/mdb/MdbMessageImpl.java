package se.plushogskolan.jetbroker.order.integration.mdb;

import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

public class MdbMessageImpl implements MdbMessage {

	@Inject
	private FlightRequestService flightRequestService;

	@Override
	public void createMockRequest() {
		flightRequestService.createFlightRequest(new FlightRequest());
	}

}
