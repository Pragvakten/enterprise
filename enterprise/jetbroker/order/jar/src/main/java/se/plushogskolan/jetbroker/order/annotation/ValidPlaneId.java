package se.plushogskolan.jetbroker.order.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidPlaneIdImpl.class)
@Documented
public @interface ValidPlaneId {

	String message() default "{validation.plane.invalid}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
