package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Local;

@Local
public interface FuelPriceService {

	String getFuelPrice();
}
