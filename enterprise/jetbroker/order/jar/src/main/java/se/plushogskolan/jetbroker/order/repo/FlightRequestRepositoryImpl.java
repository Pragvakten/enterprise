package se.plushogskolan.jetbroker.order.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;

public class FlightRequestRepositoryImpl implements FlightRequestRepository {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public long persist(FlightRequest entity) {
		manager.persist(entity);
		manager.flush();

		return entity.getId();
	}

	@Override
	public void remove(FlightRequest entity) {
		manager.remove(entity);
	}

	@Override
	public FlightRequest findById(long id) {
		return manager.find(FlightRequest.class, id);
	}

	@Override
	public void update(FlightRequest entity) {
		manager.merge(entity);
	}

	@Override
	public List<FlightRequest> getAllRequests() {
		return manager.createQuery("SELECT r FROM FlightRequest r",
				FlightRequest.class).getResultList();
	}
}
