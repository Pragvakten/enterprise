package se.plushogskolan.jetbroker.order.domain.flightrequest;

/**
 * The different statuses a flight request can be in.
 * 
 * @author fidde
 * 
 */
public enum FlightRequestStatus {
	NEW("New"), OFFER_SENT("Offer sent"), REJECTED("Rejected");

	private final String niceName;

	private FlightRequestStatus(String niceName) {
		this.niceName = niceName;
	}

	@Override
	public String toString() {
		return niceName;
	}
}
