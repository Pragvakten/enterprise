package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.airport.Airport;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;
import se.plushogskolan.jetbroker.order.domain.planetype.PlaneType;
import se.plushogskolan.jetbroker.order.repo.AirportRepository;
import se.plushogskolan.jetbroker.order.repo.PlaneRepository;
import se.plushogskolan.jetbroker.order.repo.PlaneTypeRepository;

@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	private PlaneRepository planeRepository;
	@Inject
	private AirportRepository airportRepository;
	@Inject
	private PlaneTypeRepository planeTypeRepository;

	public PlaneServiceImpl() {

	}

	public PlaneServiceImpl(AirportRepository airportRepo,
			PlaneTypeRepository typeRepo) {

		this.airportRepository = airportRepo;
		this.planeTypeRepository = typeRepo;
	}

	@Override
	public Plane getPlane(long id) {
		return planeRepository.findById(id);
	}

	@Override
	public void deletePlane(Plane plane) {
		planeRepository.remove(plane);
	}

	@Override
	public long createPlane(Plane plane) {
		return planeRepository.persist(plane);
	}

	@Override
	public void updatePlane(Plane plane) {
		planeRepository.update(plane);
	}

	@Override
	public PlaneType getPlaneType(long id) {
		return planeTypeRepository.getPlaneType(id);
	}

	@Override
	public List<PlaneType> getPlaneTypes() {
		return planeTypeRepository.getAllPlaneTypes();
	}

	@Override
	public List<Airport> getAirports() {
		return airportRepository.getAllAirports();
	}

	@Override
	public List<Plane> getAllPlanes() {
		return planeRepository.getAllPlanes();
	}

	@Override
	public PlaneType getPlaneTypeByCode(String code) {
		return planeTypeRepository.getPlaneTypeByCode(code);
	}

	@Override
	public Airport getAirportByCode(String code) {
		return airportRepository.getAirportByCode(code);
	}

}
