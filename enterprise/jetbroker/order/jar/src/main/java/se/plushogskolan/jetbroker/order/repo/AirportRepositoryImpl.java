package se.plushogskolan.jetbroker.order.repo;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.order.domain.airport.Airport;

public class AirportRepositoryImpl extends
		AbstractInMemoryRepostitory<String, Airport> implements
		AirportRepository {

	public AirportRepositoryImpl() {
		super();
		repo.put("GBG", new Airport("GBG", "Gothenburg", 454, 33));
		repo.put("LHR", new Airport("LHR", "London", 535, 34));
	}

	@Override
	public Airport getAirportByCode(String code) {
		for (Airport port : repo.values()) {
			if (port.getCode().equals(code)) {
				return port;
			}
		}
		return null;
	}

	@Override
	public List<Airport> getAllAirports() {
		return new ArrayList<Airport>(repo.values());
	}

}
