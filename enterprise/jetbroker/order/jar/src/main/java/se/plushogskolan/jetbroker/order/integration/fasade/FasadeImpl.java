package se.plushogskolan.jetbroker.order.integration.fasade;

import javax.ejb.Stateless;

import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightrequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.offer.Offer;

@Stateless
public class FasadeImpl implements Fasade {

	@Override
	public void sendConfirmation(FlightrequestConfirmation confirmation) {
		System.out.println("Confirmation sent");
	}

	@Override
	public void sendOffer(Offer offer) {
		System.out.println("Offer sent");
	}

}
