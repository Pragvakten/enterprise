package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightrequestConfirmation;
import se.plushogskolan.jetbroker.order.integration.fasade.Fasade;
import se.plushogskolan.jetbroker.order.repo.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	private Fasade mdbFlightRequestService;
	@Inject
	private FlightRequestRepository repo;

	@Override
	public FlightRequest getFlightRequest(long id) {
		return repo.findById(id);
	}

	@Override
	public void deleteFlightRequest(FlightRequest request) {
		repo.remove(request);
	}

	@Override
	public void updateFlightRequest(FlightRequest request) {
		repo.update(request);
	}

	@Override
	public long createFlightRequest(FlightRequest request) {
		mdbFlightRequestService.sendConfirmation(new FlightrequestConfirmation(
				request.getAgentID(), request.getId()));
		return repo.persist(request);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return repo.getAllRequests();
	}

}
