package se.plushogskolan.jetbroker.order.repo;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.planetype.PlaneType;

public interface PlaneTypeRepository {

	PlaneType getPlaneTypeByCode(String code);

	List<PlaneType> getAllPlaneTypes();

	PlaneType getPlaneType(long id);

}
