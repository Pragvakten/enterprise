package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Singleton aquired from the plane module. This class provides the current
 * fuelprice.
 * 
 * @author fidde
 * 
 */
@Startup
@Singleton
public class FuelPriceServiceImpl implements FuelPriceService {

	private String price;

	public FuelPriceServiceImpl() {
		setFuelPrice("7.0");
	}

	@Lock(LockType.READ)
	@Override
	public String getFuelPrice() {
		return price;
	}

	@Lock(LockType.WRITE)
	public void setFuelPrice(String price) {
		this.price = price;
	}
}
