package se.plushogskolan.jetbroker.order.integration.fasade;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightrequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.offer.Offer;

@Local
public interface Fasade {

	void sendConfirmation(FlightrequestConfirmation confirmation);

	void sendOffer(Offer offer);
}
