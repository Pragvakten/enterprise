package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.airport.Airport;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;
import se.plushogskolan.jetbroker.order.domain.planetype.PlaneType;

@Local
public interface PlaneService {

	Plane getPlane(long id);

	void deletePlane(Plane plane);

	long createPlane(Plane plane);

	void updatePlane(Plane plane);

	PlaneType getPlaneType(long id);

	Airport getAirportByCode(String code);

	List<PlaneType> getPlaneTypes();

	List<Airport> getAirports();

	List<Plane> getAllPlanes();

	PlaneType getPlaneTypeByCode(String code);
}
