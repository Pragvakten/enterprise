package se.plushogskolan.jetbroker.order.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class EditFlightRequestUtil {

	/**
	 * Calculates the traveltime by rounding up.
	 * 
	 * @param maxSpeedKm
	 * @param distanceString
	 * @return The travel time as a String
	 */
	public static String getTravelTime(int maxSpeedKm, String distanceString) {
		double distance = Double.valueOf(distanceString);
		return String.valueOf((int) Math.ceil(distance / maxSpeedKm));
	}

	/**
	 * Calculates the fuel cost
	 * 
	 * @param fuelConsumption
	 * @param distanceString
	 * @param fuelPriceString
	 * @return A String representing the fuel cost in format ### ### ###
	 */
	public static String getFuelCost(double fuelConsumption,
			String distanceString, String fuelPriceString) {

		double fuelPrice = Double.valueOf(fuelPriceString);
		double distance = Double.valueOf(distanceString);
		double result = (fuelPrice * fuelConsumption * distance);

		return formatNumber(result);
	}

	public static String formatNumber(double value) {
		DecimalFormat format = new DecimalFormat("###,###,###");

		DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(
				Locale.getDefault());
		formatSymbols.setGroupingSeparator(' ');

		format.setDecimalFormatSymbols(formatSymbols);
		return format.format(value);
	}

}
