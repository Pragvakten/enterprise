package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;

public interface FlightRequestService {

	FlightRequest getFlightRequest(long id);

	void deleteFlightRequest(FlightRequest request);

	void updateFlightRequest(FlightRequest request);

	long createFlightRequest(FlightRequest request);

	List<FlightRequest> getAllFlightRequests();

}
