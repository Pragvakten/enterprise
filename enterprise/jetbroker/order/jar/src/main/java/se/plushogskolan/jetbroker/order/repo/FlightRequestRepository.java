package se.plushogskolan.jetbroker.order.repo;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;

public interface FlightRequestRepository extends BaseRepository<FlightRequest> {

	List<FlightRequest> getAllRequests();

}
