package se.plushogskolan.jetbroker.order.domain.plane;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Entity representing a plane.
 * 
 * @author fidde
 * 
 */
@Entity
public class Plane implements IdHolder {

	@Id
	@GeneratedValue
	private long id;
	private String description;
	private String planeTypeCode;

	public Plane() {

	}

	public Plane(String description, String code) {
		setDescription(description);
		setPlaneTypeCode(code);
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	@Override
	public String toString() {
		return "Plane [id=" + id + ", description=" + description
				+ ", planeTypeCode=" + planeTypeCode + "]";
	}

}
