package se.plushogskolan.jetbroker.order.domain.flightrequest;

/**
 * Confirmation object sent back to the agent module upon recived flightrequest
 * 
 * @author fidde
 * 
 */
public class FlightrequestConfirmation {

	private long agentID;
	private long confirmationID;

	public FlightrequestConfirmation(long agentID, long confirmationID) {
		setAgentID(agentID);
		setConfirmationID(confirmationID);
	}

	public long getAgentID() {
		return agentID;
	}

	public void setAgentID(long agentID) {
		this.agentID = agentID;
	}

	public long getConfirmationID() {
		return confirmationID;
	}

	public void setConfirmationID(long confirmationID) {
		this.confirmationID = confirmationID;
	}
}
