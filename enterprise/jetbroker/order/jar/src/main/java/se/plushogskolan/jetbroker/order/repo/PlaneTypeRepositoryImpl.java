package se.plushogskolan.jetbroker.order.repo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.plushogskolan.jetbroker.order.domain.planetype.PlaneType;

public class PlaneTypeRepositoryImpl implements PlaneTypeRepository {

	private Map<Long, PlaneType> repo;

	public PlaneTypeRepositoryImpl() {
		repo = new HashMap<Long, PlaneType>();

		PlaneType type = new PlaneType("1337", "Boeing", 553, 3553, 3330, 3330);
		repo.put(0L, type);
	}

	@Override
	public PlaneType getPlaneTypeByCode(String code) {
		for (PlaneType type : repo.values()) {
			if (type.getCode().equals(code)) {
				return type;
			}
		}

		return null;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return new ArrayList<PlaneType>(repo.values());

	}

	@Override
	public PlaneType getPlaneType(long id) {
		return repo.get(id);
	}

}
