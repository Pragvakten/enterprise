package se.plushogskolan.jetbroker.order.repo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.plushogskolan.jetbroker.order.domain.plane.Plane;

public class PlaneRepositoryImpl implements PlaneRepository {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public long persist(Plane entity) {
		manager.persist(entity);
		manager.flush();

		return entity.getId();
	}

	@Override
	public void remove(Plane entity) {
		manager.remove(entity);
	}

	@Override
	public Plane findById(long id) {
		return manager.find(Plane.class, id);
	}

	@Override
	public void update(Plane entity) {
		manager.merge(entity);
	}

	@Override
	public List<Plane> getAllPlanes() {
		return manager.createQuery("SELECT p FROM Plane p", Plane.class)
				.getResultList();
	}

}
