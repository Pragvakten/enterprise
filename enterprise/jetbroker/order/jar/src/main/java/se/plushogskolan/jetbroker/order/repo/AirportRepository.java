package se.plushogskolan.jetbroker.order.repo;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.airport.Airport;

public interface AirportRepository {

	Airport getAirportByCode(String code);

	List<Airport> getAllAirports();

}
