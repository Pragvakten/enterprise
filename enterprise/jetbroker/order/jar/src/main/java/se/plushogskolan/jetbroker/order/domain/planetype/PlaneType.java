package se.plushogskolan.jetbroker.order.domain.planetype;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Entity holding data related to a type of plane. This entity is provided by
 * the plane module.
 * 
 * @author fidde
 * 
 */
public class PlaneType implements IdHolder {

	private long id;
	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;

	public PlaneType() {

	}

	public PlaneType(String code, String name, int maxNumPassangers,
			int maxRangeKm, int maxSpeedKm, double fuelConsumption) {

		setCode(code);
		setName(name);
		setMaxNoOfPassengers(maxNumPassangers);
		setMaxRangeKm(maxRangeKm);
		setMaxSpeedKmH(maxSpeedKm);
		setFuelConsumptionPerKm(fuelConsumption);
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNiceName() {
		return getName() + " " + getCode() + " (" + getMaxNoOfPassengers()
				+ " passangers)";
	}
}
