package se.plushogskolan.jetbroker.order.integration.simulator;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.integration.mdb.MdbMessage;

/**
 * Temp simulator for mocking messages
 * 
 * @author fidde
 * 
 */
@Stateless
public class MdbSimulatorImpl implements MdbSimulator {

	@Inject
	private MdbMessage mdbMessage;

	@Override
	public void createMockRequest() {
		mdbMessage.createMockRequest();
	}

}
