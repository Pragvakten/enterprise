package se.plushogskolan.jetbroker.order.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidPlaneIdImpl implements
		ConstraintValidator<ValidPlaneId, Long> {

	@Override
	public void initialize(ValidPlaneId arg0) {

	}

	@Override
	public boolean isValid(Long id, ConstraintValidatorContext arg1) {
		return id > 0;
	}

}
