package se.plushogskolan.jetbroker.order.repo;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;

public interface PlaneRepository extends BaseRepository<Plane> {

	List<Plane> getAllPlanes();

}
