package se.plushogskolan.jetbroker.order.service;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.airport.Airport;
import se.plushogskolan.jetbroker.order.domain.planetype.PlaneType;
import se.plushogskolan.jetbroker.order.repo.AirportRepository;
import se.plushogskolan.jetbroker.order.repo.PlaneTypeRepository;

public class PlaneServiceTest {

	private static final String CODE = "GBG";
	private PlaneService planeService;
	private PlaneTypeRepository typeRepo;
	private AirportRepository airportRepo;

	@Before
	public void setUp() throws Exception {
		airportRepo = EasyMock.createMock(AirportRepository.class);
		typeRepo = EasyMock.createMock(PlaneTypeRepository.class);

		planeService = new PlaneServiceImpl(airportRepo, typeRepo);
	}

	@Test
	public void testGetPlaneType() {
		EasyMock.expect(typeRepo.getPlaneType(0)).andReturn(new PlaneType());
		EasyMock.replay(typeRepo);

		planeService.getPlaneType(0);
		EasyMock.verify(typeRepo);
	}

	@Test
	public void testGetAirport() throws Exception {
		EasyMock.expect(airportRepo.getAirportByCode(CODE)).andReturn(
				new Airport());
		EasyMock.replay(airportRepo);

		planeService.getAirportByCode(CODE);
		EasyMock.verify(airportRepo);
	}
}
