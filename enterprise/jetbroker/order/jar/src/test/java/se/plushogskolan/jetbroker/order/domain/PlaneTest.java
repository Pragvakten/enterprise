package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.plane.Plane;

public class PlaneTest {

	private Plane plane;

	@Before
	public void setUp() throws Exception {
		plane = new Plane();
	}

	@Test
	public void testGetId() {
		assertEquals("id 0", 0, plane.getId());
	}

	@Test
	public void testGetDescription() throws Exception {
		assertNull("description null", plane.getDescription());
	}
}
