package se.plushogskolan.jetbroker.order.util;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.toString());

	public static Plane getPlane() {
		Plane result = new Plane();
		result.setDescription("this is a plane");

		return result;
	}

	public static Plane getPlane(String name, String code) {
		return getPlane();
	}

	public static Archive<?> createIntegrationTestArchive() {
		MavenDependencyResolver mvnResolver = DependencyResolvers.use(
				MavenDependencyResolver.class).loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "order_test.war")
				.addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml")
				.addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2")
				.resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2")
				.resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact(
				"org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}

	public static FlightRequest getFlightRequest() {
		return new FlightRequest(11, "GBG", "LHR", new DateTime());
	}

}
