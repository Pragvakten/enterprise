package se.plushogskolan.jetbroker.order;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.order.domain.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;
import se.plushogskolan.jetbroker.order.repo.PlaneRepository;
import se.plushogskolan.jetbroker.order.util.TestFixture;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class PlaneRepositoryIntegrationTest extends
		AbstractRepositoryTest<Plane, PlaneRepository> {

	@Inject
	private PlaneRepository repo;

	@Override
	protected PlaneRepository getRepository() {
		return repo;
	}

	@Override
	protected Plane getEntity1() {
		return TestFixture.getPlane("foo", "LOL");
	}

	@Override
	protected Plane getEntity2() {
		return TestFixture.getPlane("bar", "KEK");
	}

	@Test
	public void testGetAllPlanes() throws Exception {
		List<Plane> list = repo.getAllPlanes();
		assertEquals("list empty", 0, list.size());
	}

}
