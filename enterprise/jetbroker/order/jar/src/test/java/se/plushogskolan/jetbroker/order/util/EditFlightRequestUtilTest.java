package se.plushogskolan.jetbroker.order.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EditFlightRequestUtilTest {

	@Test
	public void testGetTravelTime() {
		assertEquals("travel time is 1", "1",
				EditFlightRequestUtil.getTravelTime(1, "1"));
	}

	@Test
	public void testGetFuelCost() throws Exception {
		assertEquals("fuelCost is ", "1",
				EditFlightRequestUtil.getFuelCost(1d, "1", "1"));
	}

}
