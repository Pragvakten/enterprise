package se.plushogskolan.jetbroker.order.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class FuelPriceServiceTest {

	private FuelPriceService fuelPriceService;

	@Before
	public void setUp() throws Exception {
		fuelPriceService = new FuelPriceServiceImpl();
	}

	@Test
	public void testGetFuelPrice() {
		assertNotNull("fuelprice not null", fuelPriceService.getFuelPrice());
	}
}
