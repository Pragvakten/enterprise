package se.plushogskolan.jetbroker.order;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.order.domain.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.order.repo.FlightRequestRepository;
import se.plushogskolan.jetbroker.order.util.TestFixture;

@RunWith(Arquillian.class)
@Transactional(value = TransactionMode.ROLLBACK)
public class FlightRequestRepositoryIntegrationTest extends
		AbstractRepositoryTest<FlightRequest, FlightRequestRepository> {

	@Inject
	private FlightRequestRepository repository;

	@Override
	protected FlightRequestRepository getRepository() {
		return repository;
	}

	@Override
	protected FlightRequest getEntity1() {
		return TestFixture.getFlightRequest();
	}

	@Override
	protected FlightRequest getEntity2() {
		return TestFixture.getFlightRequest();
	}

	@Test
	public void testGetAllRequests() throws Exception {
		List<FlightRequest> list = repository.getAllRequests();
		assertEquals("list empty", 0, list.size());
	}
}
