<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />
<title>Edit flightrequest</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<h1>Edit flight request</h1>
	<div class="width-large">
		<div>
			<span class="bolder">ID</span> <span class="floatRight alignRight">${flightRequestBean.flightRequest.id}</span>
		</div>
		<div class="clear">
			<span class="bolder">Departure Airport</span> <span
				class="floatRight alignRight">${flightRequestBean.departureAirport.code},
				${flightRequestBean.departureAirport.name}</span>
		</div>
		<div class="clear">
			<span class="bolder">Arrival Airport</span> <span class="floatRight alignRight">${flightRequestBean.arrivalAirport.code},
				${flightRequestBean.arrivalAirport.name }</span>
		</div>
		<div class="clear">
			<span class="bolder">Date</span> <span class="floatRight alignRight">${flightRequestBean.flightRequest.niceDate}</span>
		</div>
		<div class="clear">
			<span class="bolder">No of passangers</span> <span class="floatRight alignRight">${flightRequestBean.flightRequest.numPassangers}</span>
		</div>
		<div class="clear">
			<span class="bolder">Distance (km)</span> <span class="floatRight alignRight">${flightRequestBean.distance}</span>
		</div>
		<br>
		<div class="clear">
			<span class="bolder">Fuel cost (liter)</span> <span class="floatRight alignRight">${flightRequestBean.fuelPrice}</span>
		</div>
	</div>
	<h1 class="clear">Our offer to customer</h1>
	<form:form action="/editFlightRequest.html?id=${flightRequestBean.flightRequest.id}" method="POST" commandName="offer">
	<form:errors class="errors" path="planeId"></form:errors>
		<table class="dataTable">
			<thead>
				<th></th>
				<th>Plane</th>
				<th>No of passangers</th>
				<th>Max speed (km/h)</th>
				<th>Fuel consumption (l/km)</th>
				<th>Total fuelcost</th>
				<th>Travel time (min)</th>
			</thead>
			<c:forEach items="${planes}" var="plane">
				<tr>
					<td><form:radiobutton path="planeId" value="${plane.id}" /></td>
					<td>${plane.planeType.name} ${plane.planeType.code}</td>
					<td>${plane.planeType.maxNoOfPassengers}</td>
					<td>${plane.planeType.maxSpeedKmH}</td>
					<td>${plane.planeType.fuelConsumptionPerKm}</td>
					<td>${plane.fuelCost} SEK</td>
					<td>${plane.travelTime}</td>
				</tr>
			</c:forEach>
		</table>
		<form:errors class="errors" path="offeredPrice"></form:errors>
		<div>
			<form:label path="offeredPrice">
			Offered price (SEK):
			<form:input path="offeredPrice" type="text" value="0.0" />
			</form:label>
		</div>
		<div>
			<form:input path="" type="submit" value="Submit offer" />
			<a href="<%=request.getContextPath()%>/editFlightRequest.html?rejected=1&id=${flightRequestBean.flightRequest.id}">Reject offer </a> <a
				href="<%=request.getContextPath()%>/index.html"> Cancel</a>
		</div>
	</form:form>
</body>
</html>