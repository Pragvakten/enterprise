<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/style/common.css">
<title>Plane</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div>
		<h2>
			<img src="<%=request.getContextPath()%>/images/plane.png">Edit
			plane
		</h2>
		<hr>
	</div>
	<div id="formDiv">
		<form:form commandName="plane" method="POST" action="/plane.html">
			<div>
				<label>ID</label> 
				<span>${plane.id}</span>
			</div>
			<div class="clear">
				<form:input path="id" type="hidden" value="${plane.id}" />
				<form:label path="planeType">Plane type</form:label>
				<form:select path="planeTypeCode">
					<form:option value=""></form:option>
					<form:options items="${planeTypes }" itemLabel="niceName"
						itemValue="code" />
				</form:select>
			</div>
			<div class="clear">
				<form:label path="description">Description</form:label>
				<form:textarea path="description" value="${plane.description }" />
			</div>
			<div class="clear">
				<input type="submit" value="Submit" /> <a
					href="<%=request.getContextPath()%>/index.html">Cancel</a>
			</div>
		</form:form>
	</div>
</body>
</html>