<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

</head>
<body>
	<jsp:include page="header.jsp" />
	<!-- Request table -->
	<div>
		<h3>
			<img src="<%=request.getContextPath()%>/images/flightrequest.png" />Flight
			requests
		</h3>
		<a href="<%=request.getContextPath()%>/mock.html">Mock</a>
		<hr>
		<table class="dataTable">
			<thead>
				<th>ID</th>
				<th>Status</th>
				<th>Departure airport</th>
				<th>Arrival airport</th>
				<th>Date</th>
				<th>No passangers</th>
				<th>Offered price</th>
				<th>Offered plane</th>
				<th>Edit</th>
			</thead>
			<c:forEach items="${requests }" var="request">
				<tr>
					<td>${request.flightRequest.id}</td>
					<td>${request.flightRequest.statusNiceName}</td>
					<td>${request.departureAirport.name}
						(${request.departureAirport.code})</td>
					<td>${request.arrivalAirport.name}
						(${request.arrivalAirport.code})</td>
					<td>${request.flightRequest.niceDate}</td>
					<td>${request.flightRequest.numPassangers}</td>

					<c:choose>
						<c:when test="${request.flightRequest.offer.offeredPrice == null}">
							<td>0 SEK</td>
						</c:when>
						<c:otherwise>
							<td>${request.offerNicePrice} SEK</td>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${request.planeType != null}">
							<td>${request.planeType.name} ${request.planeType.code}
								(${request.planeType.maxNoOfPassengers} seats)</td>
						</c:when>
						<c:otherwise>
							<td></td>
						</c:otherwise>
					</c:choose>
					<td><a
						href="<%=request.getContextPath()%>/editFlightRequest.html?id=${request.flightRequest.id}"><img
							src="<%=request.getContextPath()%>/images/edit.png"></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<!-- end Request table -->
	<!-- Our planes table -->
	<div>
		<h3>
			<img src="<%=request.getContextPath()%>/images/plane.png" />Our
			planes
		</h3>
		<hr>
		<div class="margin-small">
			<a class="button" href="<%=request.getContextPath()%>/plane.html">Create
				new plane</a>
		</div>
		<table class="dataTable">
			<thead>
				<th>ID</th>
				<th>Code</th>
				<th>Name</th>
				<th>Max passangers</th>
				<th>Range (km)</th>
				<th>Max speed (km/h)</th>
				<th>Description</th>
				<th>Edit</th>
			</thead>
			<c:forEach items="${planes }" var="plane">
				<tr>
					<td>${plane.id}</td>
					<td>${plane.code}</td>
					<td>${plane.planeType.name} ${plane.planeType.code}</td>
					<td>${plane.planeType.maxNoOfPassengers}</td>
					<td>${plane.planeType.maxRangeKm}</td>
					<td>${plane.planeType.maxSpeedKmH}</td>
					<td>${plane.description}</td>
					<td><a
						href="<%=request.getContextPath()%>/plane.html?id=${plane.id}"><img
							src="<%=request.getContextPath()%>/images/edit.png"></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<!-- end our planes table -->
	<!-- fuel div-->
	<div>
		<h2>
			<img src="<%=request.getContextPath()%>/images/gas.png" />Fuel price
		</h2>
		<hr>
		<p>Current cost is ${fuelprice} SEK</p>
	</div>
	<!-- end fuel div-->
</body>

</html>