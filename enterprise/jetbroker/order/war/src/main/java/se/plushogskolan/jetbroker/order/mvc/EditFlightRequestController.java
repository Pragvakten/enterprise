package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.bean.FlightRequestBean;
import se.plushogskolan.jetbroker.order.bean.PlaneBean;
import se.plushogskolan.jetbroker.order.domain.airport.Airport;
import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.domain.offer.Offer;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;
import se.plushogskolan.jetbroker.order.integration.fasade.Fasade;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.util.EditFlightRequestUtil;

@Controller
@RequestMapping("/editFlightRequest.html")
public class EditFlightRequestController {

	@Inject
	private FlightRequestService flightRequestService;
	@Inject
	private PlaneService planeService;
	@Inject
	private FuelPriceService fuelPriceService;
	@Inject
	private Fasade mdbFlightRequestService;

	@RequestMapping(method = RequestMethod.GET, params = { "rejected" })
	public ModelAndView rejectEditRequest(@RequestParam long id,
			@RequestParam String rejected) {
		ModelAndView modelAndView = new ModelAndView("redirect:index.html");

		FlightRequest flightRequest = flightRequestService.getFlightRequest(id);

		flightRequest.setStatus(FlightRequestStatus.REJECTED);
		flightRequestService.updateFlightRequest(flightRequest);

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView postEditRequest(@RequestParam long id,
			@Valid Offer offer, BindingResult bindingResult) {

		if (bindingResult.hasErrors() || offer.getPlaneId() < 1) {
			ModelAndView view = new ModelAndView("flightrequest");
			initView(id, view);
			view.addObject("offer", offer);
			return view;
		}

		ModelAndView modelAndView = new ModelAndView("redirect:index.html");

		mdbFlightRequestService.sendOffer(offer);

		FlightRequest flightRequest = flightRequestService.getFlightRequest(id);

		flightRequest.setOffer(offer);
		flightRequest.setStatus(FlightRequestStatus.OFFER_SENT);
		flightRequestService.updateFlightRequest(flightRequest);

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getEditRequest(@RequestParam long id, Offer offer) {
		ModelAndView modelAndView = new ModelAndView("flightrequest");

		initView(id, modelAndView);
		modelAndView.addObject("offer", offer);
		return modelAndView;
	}

	private void initView(long id, ModelAndView modelAndView) {
		FlightRequest flightRequest = flightRequestService.getFlightRequest(id);
		Airport departureAirport = planeService.getAirportByCode(flightRequest
				.getDepartureAirport());
		Airport arrivalAirport = planeService.getAirportByCode(flightRequest
				.getArrivalAirport());

		FlightRequestBean bean = new FlightRequestBean(flightRequest,
				departureAirport, arrivalAirport);

		bean.setFuelPrice(fuelPriceService.getFuelPrice());
		modelAndView.addObject("flightRequestBean", bean);

		modelAndView.addObject("planes", getPlaneBeans(bean));
	}

	private List<PlaneBean> getPlaneBeans(FlightRequestBean flightRequestBean) {
		List<PlaneBean> beans = new ArrayList<PlaneBean>();
		PlaneBean planeBean;

		for (Plane plane : planeService.getAllPlanes()) {
			planeBean = new PlaneBean();

			planeBean.setDescription(plane.getDescription());
			planeBean.setId(String.valueOf(plane.getId()));
			planeBean.setPlaneType(planeService.getPlaneTypeByCode(plane
					.getPlaneTypeCode()));

			String code = planeBean.getPlaneType().getName().substring(0, 1)
					+ planeBean.getPlaneType().getCode();

			planeBean.setCode(code);

			planeBean.setFuelCost(EditFlightRequestUtil.getFuelCost(planeBean
					.getPlaneType().getFuelConsumptionPerKm(),
					flightRequestBean.getDistance(), fuelPriceService
							.getFuelPrice()));

			planeBean.setTravelTime(EditFlightRequestUtil.getTravelTime(
					planeBean.getPlaneType().getMaxSpeedKmH(),
					flightRequestBean.getDistance()));

			beans.add(planeBean);
		}
		return beans;
	}

}
