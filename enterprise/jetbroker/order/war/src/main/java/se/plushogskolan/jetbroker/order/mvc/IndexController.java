package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.bean.FlightRequestBean;
import se.plushogskolan.jetbroker.order.bean.PlaneBean;
import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class IndexController {

	@Inject
	private PlaneService planeService;
	@Inject
	private FuelPriceService fuelPriceService;
	@Inject
	private FlightRequestService flightRequestService;

	@RequestMapping("/index.html")
	public ModelAndView index() {

		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("fuelprice", fuelPriceService.getFuelPrice());

		List<FlightRequestBean> flightRequestBeans = getFlightRequestBeans();
		modelAndView.addObject("requests", flightRequestBeans);

		List<PlaneBean> beans = getPlaneBeans();
		modelAndView.addObject("planes", beans);

		return modelAndView;
	}

	private List<FlightRequestBean> getFlightRequestBeans() {
		List<FlightRequestBean> result = new ArrayList<FlightRequestBean>();
		FlightRequestBean bean;

		for (FlightRequest request : flightRequestService
				.getAllFlightRequests()) {

			bean = new FlightRequestBean(
					request,
					planeService.getAirportByCode(request.getDepartureAirport()),
					planeService.getAirportByCode(request.getArrivalAirport()));

			if (request.getOffer() != null) {
				Plane plane = planeService.getPlane(request
						.getOffer().getPlaneId());

				bean.setOfferedPlane(plane);
				bean.setPlaneType(planeService.getPlaneTypeByCode(plane
						.getPlaneTypeCode()));
			}

			result.add(bean);
		}
		return result;
	}

	private List<PlaneBean> getPlaneBeans() {
		List<PlaneBean> beans = new ArrayList<PlaneBean>();
		PlaneBean bean;

		for (Plane plane : planeService.getAllPlanes()) {
			bean = new PlaneBean();

			bean.setDescription(plane.getDescription());
			bean.setId(String.valueOf(plane.getId()));
			bean.setPlaneType(planeService.getPlaneTypeByCode(plane
					.getPlaneTypeCode()));

			String code = bean.getPlaneType().getName().substring(0, 1)
					+ bean.getPlaneType().getCode();

			bean.setCode(code);
			beans.add(bean);
		}
		return beans;
	}

}
