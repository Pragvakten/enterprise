package se.plushogskolan.jetbroker.order.bean;

import se.plushogskolan.jetbroker.order.domain.planetype.PlaneType;

public class PlaneBean {

	private String id;
	private PlaneType planeType;
	private String planeTypeCode;
	private String description;
	private String code;
	private String fuelCost;
	private String travelTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PlaneType getPlaneType() {
		return planeType;
	}

	public void setPlaneType(PlaneType planeType) {
		this.planeType = planeType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	public String getFuelCost() {
		return fuelCost;
	}

	public void setFuelCost(String fuelCost) {
		this.fuelCost = fuelCost;
	}

	public String getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(String travelTime) {
		this.travelTime = travelTime;
	}

}
