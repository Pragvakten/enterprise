package se.plushogskolan.jetbroker.order.mvc;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.integration.simulator.MdbSimulator;

@Controller
@RequestMapping("/mock.html")
public class MockFlightrequestController {

	@Inject
	private MdbSimulator mdbSimulator;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getMockFlightrequest() {

		return new ModelAndView("mockFlightrequest");
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView postMockFlightrequest() {

		mdbSimulator.createMockRequest();

		return new ModelAndView("redirect:index.html");
	}
}
