package se.plushogskolan.jetbroker.order.mvc;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.bean.PlaneBean;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
@RequestMapping("/plane.html")
public class PlaneController {

	@Inject
	private PlaneService planeService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getPlane(PlaneBean bean) {
		ModelAndView modelAndView = new ModelAndView("plane");
		modelAndView.addObject("planeTypes", planeService.getPlaneTypes());
		modelAndView.addObject("plane", bean);

		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET, params = { "id" })
	public ModelAndView editPlane(@RequestParam long id, PlaneBean bean) {
		Plane plane = planeService.getPlane(id);

		bean.setDescription(plane.getDescription());
		bean.setId(String.valueOf(plane.getId()));
		bean.setPlaneType(planeService.getPlaneTypeByCode(plane
				.getPlaneTypeCode()));

		ModelAndView result = new ModelAndView("plane");
		result.addObject("plane", bean);
		result.addObject("planeTypes", planeService.getPlaneTypes());

		return result;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String postPlane(PlaneBean bean) {

		Plane plane = new Plane(bean.getDescription(), bean.getPlaneTypeCode());

		if (bean.getId().equals("")) {
			planeService.createPlane(plane);

		} else {
			plane.setId(Long.valueOf(bean.getId()));
			planeService.updatePlane(plane);
		}

		return "redirect:index.html";
	}
}
