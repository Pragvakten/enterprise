package se.plushogskolan.jetbroker.order.bean;

import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.airport.Airport;
import se.plushogskolan.jetbroker.order.domain.flightrequest.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.plane.Plane;
import se.plushogskolan.jetbroker.order.domain.planetype.PlaneType;
import se.plushogskolan.jetbroker.order.util.EditFlightRequestUtil;

public class FlightRequestBean {

	private FlightRequest flightRequest;
	private Airport departureAirport;
	private Airport arrivalAirport;
	private String fuelPrice;
	private String travelDistance;
	private long planeId;
	private Plane offeredPlane;
	private PlaneType planeType;

	public FlightRequestBean() {

	}

	public FlightRequestBean(FlightRequest flightRequest,
			Airport departureAirport, Airport arrivalAirport) {

		setFlightRequest(flightRequest);
		setDepartureAirport(departureAirport);
		setArrivalAirport(arrivalAirport);
	}

	public String getOfferNicePrice() {
		double result = Double.valueOf(flightRequest.getOffer()
				.getOfferedPrice());
		return EditFlightRequestUtil.formatNumber(result);
	}

	public String getDistance() {
		double lat1 = departureAirport.getLatitude();
		double lng1 = departureAirport.getLongitude();
		double lat2 = arrivalAirport.getLatitude();
		double lng2 = arrivalAirport.getLongitude();

		return String.format("%.2f",
				HaversineDistance.getDistance(lat1, lng1, lat2, lng2));
	}

	public FlightRequest getFlightRequest() {
		return flightRequest;
	}

	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	}

	public Airport getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public String getFuelPrice() {
		return fuelPrice;
	}

	public void setFuelPrice(String fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public String getTravelDistance() {
		return travelDistance;
	}

	public void setTravelDistance(String travelDistance) {
		this.travelDistance = travelDistance;
	}

	public long getPlaneId() {
		return planeId;
	}

	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}

	public Plane getOfferedPlane() {
		return offeredPlane;
	}

	public void setOfferedPlane(Plane offeredPlane) {
		this.offeredPlane = offeredPlane;
	}

	public PlaneType getPlaneType() {
		return planeType;
	}

	public void setPlaneType(PlaneType planeType) {
		this.planeType = planeType;
	}

}
