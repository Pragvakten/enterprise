package se.plushogskolan.jetbroker.plane.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongitudeValidator extends AbstractLongitudeAndLatitudeValidator
		implements ConstraintValidator<Longitude, Object> {

	@Override
	public void initialize(Longitude arg0) {

	}

	@Override
	public boolean isValid(Object longitude, ConstraintValidatorContext arg1) {
		return isValid(longitude, 180);
	}

}
