package se.plushogskolan.jetbroker.plane.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.repository.PlaneRepository;

public class JpaPlaneRepository implements PlaneRepository {

	@PersistenceContext
	protected EntityManager em;

	@Override
	public PlaneType getPlaneType(long id) {
		return em.find(PlaneType.class, id);
	}

	@Override
	public long createPlaneType(PlaneType planeType) {
		em.persist(planeType);
		return planeType.getId();
	}

	@Override
	public void updatePlaneType(PlaneType planeType) {
		em.merge(planeType);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return em.createQuery("select pt from PlaneType pt").getResultList();
	}

}
