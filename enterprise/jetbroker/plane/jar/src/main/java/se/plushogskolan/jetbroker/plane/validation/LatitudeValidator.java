package se.plushogskolan.jetbroker.plane.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LatitudeValidator extends AbstractLongitudeAndLatitudeValidator
		implements ConstraintValidator<Latitude, Object> {

	@Override
	public void initialize(Latitude arg0) {

	}

	@Override
	public boolean isValid(Object lat, ConstraintValidatorContext arg1) {
		return isValid(lat, 90);
	}
}
