package se.plushogskolan.jetbroker.plane.validation;

public class AbstractLongitudeAndLatitudeValidator {

	public boolean isValid(Object obj, int val) {
		Double doubleobj;
		if (obj instanceof Double) {
			doubleobj = (Double) obj;

			return checkValue(val, doubleobj);

		} else if (obj instanceof String) {
			try {
				String stringobj = (String) obj;
				doubleobj = (Double) Double.valueOf(stringobj);

				return checkValue(val, doubleobj);

			} catch (NumberFormatException e) {
				return false;
			}

		} else if (obj instanceof Integer) {
			Integer intobj = (Integer) obj;
			return intobj.intValue() >= -val && intobj.intValue() <= val
					&& intobj.intValue() != 0;

		} else {
			return false;
		}
	}

	private boolean checkValue(int val, Double doubleobj) {
		return doubleobj.doubleValue() >= -val
				&& doubleobj.doubleValue() <= val
				&& doubleobj.doubleValue() != 0;
	}
}
