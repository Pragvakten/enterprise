package se.plushogskolan.jetbroker.plane.repository;

import java.util.List;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;

public interface PlaneRepository {

	PlaneType getPlaneType(long id);

	long createPlaneType(PlaneType planeType);

	void updatePlaneType(PlaneType planeType);

	List<PlaneType> getAllPlaneTypes();

}
