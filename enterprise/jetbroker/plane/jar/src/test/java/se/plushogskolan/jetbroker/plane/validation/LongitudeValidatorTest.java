package se.plushogskolan.jetbroker.plane.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LongitudeValidatorTest {

	private Object longitude;

	@Test
	public void testIsValid_true() {
		LongitudeValidator validator = new LongitudeValidator();

		longitude = new String("180");
		assertTrue(validator.isValid(longitude, null));

		longitude = new Double(180.0);
		assertTrue(validator.isValid(longitude, null));

		longitude = new Integer(180);
		assertTrue(validator.isValid(longitude, null));

		longitude = new String("-180");
		assertTrue(validator.isValid(longitude, null));

		longitude = new Double(-180.0);
		assertTrue(validator.isValid(longitude, null));

		longitude = new Integer(-180);
		assertTrue(validator.isValid(longitude, null));
	}

	@Test
	public void testIsValid_false() {
		LongitudeValidator validator = new LongitudeValidator();

		longitude = new String("-181");
		assertFalse(validator.isValid(longitude, null));

		longitude = new Double(-184.0);
		assertFalse(validator.isValid(longitude, null));

		longitude = new Integer(255);
		assertFalse(validator.isValid(longitude, null));

		longitude = new String("255");
		assertFalse(validator.isValid(longitude, null));

		longitude = new Double(255.0);
		assertFalse(validator.isValid(longitude, null));

		longitude = new Integer(255);
		assertFalse(validator.isValid(longitude, null));

		longitude = new String("0");
		assertFalse(validator.isValid(longitude, null));

		longitude = new Double(0.0);
		assertFalse(validator.isValid(longitude, null));

		longitude = new Integer(0);
		assertFalse(validator.isValid(longitude, null));
	}
}
