package se.plushogskolan.jetbroker.plane.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LatitudeValidatorTest {
	private Object latitude;

	@Test
	public void testIsValid_true() {
		LatitudeValidator validator = new LatitudeValidator();

		latitude = new String("90");
		assertTrue(validator.isValid(latitude, null));

		latitude = new Double(90.0);
		assertTrue(validator.isValid(latitude, null));

		latitude = new Integer(90);
		assertTrue(validator.isValid(latitude, null));

		latitude = new String("-90");
		assertTrue(validator.isValid(latitude, null));

		latitude = new Double(-90.0);
		assertTrue(validator.isValid(latitude, null));

		latitude = new Integer(-90);
		assertTrue(validator.isValid(latitude, null));
	}

	@Test
	public void testIsValid_false() {
		LatitudeValidator validator = new LatitudeValidator();

		latitude = new String("-181");
		assertFalse(validator.isValid(latitude, null));

		latitude = new Double(-184.0);
		assertFalse(validator.isValid(latitude, null));

		latitude = new Integer(-184);
		assertFalse(validator.isValid(latitude, null));

		latitude = new String("181");
		assertFalse(validator.isValid(latitude, null));

		latitude = new Double(184.0);
		assertFalse(validator.isValid(latitude, null));

		latitude = new Integer(184);
		assertFalse(validator.isValid(latitude, null));

		latitude = new String("0");
		assertFalse(validator.isValid(latitude, null));

		latitude = new Double(0.0);
		assertFalse(validator.isValid(latitude, null));

		latitude = new Integer(0);
		assertFalse(validator.isValid(latitude, null));
	}
}
