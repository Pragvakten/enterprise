package se.plushogskolan.jetbroker;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;

import se.plushogskolan.jetbroker.plane.domain.Airport;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());

	public static Airport getValidAirport(long id, String code, String name) {
		Airport airport = new Airport();
		airport.setId(id);
		airport.setCode(code);
		airport.setName(name);
		airport.setLatitude(10);
		airport.setLongitude(20);
		return airport;
	}

	public static Airport getValidAirport() {
		return getValidAirport(1, "GBG", "Gothenburg City airport");
	}

	public static Airport getValidAirport(String code, String name) {
		return getValidAirport(0, code, name);
	}

	public static Archive<?> createIntegrationTestArchive() {
		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "plane_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}
}
