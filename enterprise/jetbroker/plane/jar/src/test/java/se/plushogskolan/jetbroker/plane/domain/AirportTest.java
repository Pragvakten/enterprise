package se.plushogskolan.jetbroker.plane.domain;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class AirportTest {

	@Test
	public void testValidation_name_empty() {
		Airport airport = TestFixture.getValidAirport();
		airport.setName("");
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(
				airport);
		assertPropertyIsInvalid("name", violations);
	}

	@Test
	public void testValidation_name_null() {
		Airport airport = TestFixture.getValidAirport();
		airport.setName(null);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(
				airport);
		assertPropertyIsInvalid("name", violations);
	}

	@Test
	public void testValidation_name_ok() {
		Airport airport = TestFixture.getValidAirport();
		airport.setName("City Airport");
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(
				airport);
		assertTrue("No validation errors for name", violations.isEmpty());
	}

	/**
	 * Tests that the validation for airport code is active
	 */
	@Test
	public void testValidation_code_active() {

		// Airport not OK
		Airport airport = TestFixture.getValidAirport();
		airport.setCode("");
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(
				airport);
		assertPropertyIsInvalid("code", violations);

		// Airport OK
		airport.setCode("GBG");
		violations = getValidator().validate(airport);
		assertTrue("No validation errors for code", violations.isEmpty());
	}

	/**
	 * Test that the latitude validation is active
	 */
	@Test
	public void testValidation_latitude_active() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLatitude(0);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(
				airport);
		assertPropertyIsInvalid("latitude", violations);

		airport.setLatitude(10);
		violations = getValidator().validate(airport);
		assertTrue("No validation errors for latitude", violations.isEmpty());

	}

	/**
	 * Test that the longitude validation is active
	 */
	@Test
	public void testValidation_longitude_active() {
		Airport airport = TestFixture.getValidAirport();
		airport.setLongitude(0);
		Set<ConstraintViolation<Airport>> violations = getValidator().validate(
				airport);
		assertPropertyIsInvalid("longitude", violations);

		airport.setLongitude(10);
		violations = getValidator().validate(airport);
		assertTrue("No validation errors for longitude", violations.isEmpty());
	}

	/**
	 * Helper method to check if a certain property has reported errors
	 */
	protected void assertPropertyIsInvalid(String property,
			Set<ConstraintViolation<Airport>> violations) {

		boolean errorFound = false;
		for (ConstraintViolation<Airport> constraintViolation : violations) {
			if (constraintViolation.getPropertyPath().toString()
					.equals(property)) {
				errorFound = true;
				break;
			}
		}

		if (!errorFound) {
			fail("Expected validation error for '" + property
					+ "', but no such error exists");
		}
	}

	protected Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}
}
