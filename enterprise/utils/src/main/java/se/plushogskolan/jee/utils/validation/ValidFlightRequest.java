package se.plushogskolan.jee.utils.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FlightRequestValidator.class)
@Documented
public @interface ValidFlightRequest {

	String message() default "{validation.flightrequest.airport.invalid}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
