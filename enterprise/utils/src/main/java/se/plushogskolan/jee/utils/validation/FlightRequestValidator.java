package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FlightRequestValidator implements
		ConstraintValidator<ValidFlightRequest, ArrivalAndDepartureAirportHolder> {

	@Override
	public void initialize(ValidFlightRequest arg0) {

	}

	@Override
	public boolean isValid(ArrivalAndDepartureAirportHolder holder,
			ConstraintValidatorContext arg1) {

		if (holder.getArrivalAirportCode() == null
				|| holder.getDepartureAirportCode() == null) {
			return false;

		} else {
			return !holder.getArrivalAirportCode().equals(
					holder.getDepartureAirportCode());
		}
	}

}
