package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

public class AirportCodeValidator implements
		ConstraintValidator<AirportCode, String> {

	@Override
	public void initialize(AirportCode code) {

	}

	@Override
	public boolean isValid(String code, ConstraintValidatorContext context) {

		if (StringUtils.isEmpty(code)) {
			return false;
		}

		return code.length() == 3;
	}

}
