package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class AirportCodeValidatorTest {

	private AirportCodeValidator validator;

	@Before
	public void setup() {
		validator = new AirportCodeValidator();

	}

	@Test
	public void testIsValid_ok() {
		assertTrue("OK", validator.isValid("AAA", null));
	}

	@Test
	public void testIsValid_null() {
		assertFalse("Null", validator.isValid(null, null));
	}

	@Test
	public void testIsValid_empty() {
		assertFalse("", validator.isValid(null, null));
	}

	@Test
	public void testIsValid_tooShort() {
		assertFalse("AA", validator.isValid(null, null));
	}

	@Test
	public void testIsValid_tooLong() {
		assertFalse("AAAA", validator.isValid(null, null));
	}
}
