package se.plushogskolan.jee.exercises.localization;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleExercise {

	private static final String LOCALIZATIONEXERCISE = "localizationexercise";

	public LocaleExercise() {

	}

	public String sayHello(Locale locale) {
		return ResourceBundle.getBundle(LOCALIZATIONEXERCISE, locale)
				.getString("hello");
	}

	public String sayName(String string, Locale locale) {
		return ResourceBundle.getBundle(LOCALIZATIONEXERCISE, locale)
				.getString("hello") + " " + string;
	}

	public String saySomethingEnglish(Locale locale) {
		return ResourceBundle.getBundle(LOCALIZATIONEXERCISE, locale)
				.getString("onlyEnglish");
	}

	public String introduceYourself(int age, String city, String name,
			Locale locale) {

		MessageFormat format = new MessageFormat(ResourceBundle.getBundle(
				LOCALIZATIONEXERCISE, locale).getString("introductions"),
				locale);

		return format.format(new Object[] { name, age, city });
	}

	public String sayApointmentDate(Date date, Locale locale) {
		MessageFormat format = new MessageFormat(ResourceBundle.getBundle(
				LOCALIZATIONEXERCISE, locale).getString("appointmentDate"),
				locale);

		Object[] text = new Object[] { date };

		return format.format(text);
	}

	public String sayPrice(double price, Locale locale) {

		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
		return numberFormat.format(price);
	}

	public String sayFractionDigits(double number) {
		DecimalFormat decimalFormat = new DecimalFormat("#.000");
		return decimalFormat.format(number);
	}
}
