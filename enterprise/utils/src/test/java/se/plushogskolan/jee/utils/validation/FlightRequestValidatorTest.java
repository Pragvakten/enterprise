package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FlightRequestValidatorTest {

	@Test
	public void testIsValid_false() {
		ArrivalAndDepartureAirportHolder holder = new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return "GBG";
			}

			@Override
			public String getArrivalAirportCode() {
				return "GBG";
			}
		};
		FlightRequestValidator validator = new FlightRequestValidator();
		assertFalse("not valid", validator.isValid(holder, null));

	}

	@Test
	public void testIsValid_true() {
		ArrivalAndDepartureAirportHolder holder = new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return "GBG";
			}

			@Override
			public String getArrivalAirportCode() {
				return "LHR";
			}
		};
		FlightRequestValidator validator = new FlightRequestValidator();
		assertTrue("valid", validator.isValid(holder, null));

	}
}
