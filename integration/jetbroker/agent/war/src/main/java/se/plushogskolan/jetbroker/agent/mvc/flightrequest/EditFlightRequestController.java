package se.plushogskolan.jetbroker.agent.mvc.flightrequest;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.services.AirPortService;
import se.plushogskolan.jetbroker.agent.services.CustomerService;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@Controller
@RequestMapping("/editFlightRequest/{requestId}.html")
public class EditFlightRequestController {

	Logger log = Logger.getLogger(EditFlightRequestController.class.getName());

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private AirPortService airPortService;

	@Inject
	private CustomerService customerService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long requestId) {

		log.fine("Edit flight request, id=" + requestId);

		EditFlightRequestBean bean = new EditFlightRequestBean();
		boolean isNewRequest = requestId <= 0;

		FlightRequest request = null;
		if (isNewRequest) {
			request = new FlightRequest();
			request.setRequestStatus(FlightRequestStatus.CREATED);
		} else {
			request = getFlightRequestService().getFlightRequest(requestId);
		}
		bean.copyFlightRequestValuesToBean(request);

		ModelAndView mav = new ModelAndView("flightrequest/editFlightRequest");
		mav.addObject("editFlightRequestBean", bean);
		addReferenceData(mav, requestId);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditFlightRequestBean bean, BindingResult errors) throws Exception {

		if (errors.hasErrors()) {

			ModelAndView mav = new ModelAndView("flightrequest/editFlightRequest");
			long requestId = bean.getId();
			mav.addObject("editFlightRequestBean", bean);
			addReferenceData(mav, requestId);
			return mav;
		}

		FlightRequest request = null;
		if (bean.getId() > 0) {
			request = getFlightRequestService().getFlightRequest(bean.getId());
		} else {
			request = new FlightRequest();
		}

		Customer customer = getCustomerService().getCustomer(bean.getCustomerId());
		bean.copyBeanValuesToFlightRequest(request, customer);

		if (request.getId() > 0) {
			getFlightRequestService().updateFlightRequest(request);
		} else {
			getFlightRequestService().createFlightRequest(request);
		}

		return new ModelAndView("redirect:/index.html");
	}

	/**
	 * Utility method for preparing the data needed to fill the form.
	 */
	protected void addReferenceData(ModelAndView mav, long requestId) {

		mav.addObject("isNewRequest", requestId <= 0);
		mav.addObject("airports", getAirPortService().getAllAirPorts());
		mav.addObject("customers", getCustomerService().getAllCustomers());
		mav.addObject("hours", getHours());
		mav.addObject("minutes", getMinutes());
	}

	private List<Integer> getMinutes() {
		List<Integer> minutes = new ArrayList<Integer>();
		for (int i = 0; i < 60; i++) {
			minutes.add(i);
		}
		return minutes;
	}

	private List<Integer> getHours() {
		List<Integer> hours = new ArrayList<Integer>();
		for (int i = 0; i < 24; i++) {
			hours.add(i);
		}
		return hours;
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

	public AirPortService getAirPortService() {
		return airPortService;
	}

	public void setAirPortService(AirPortService airPortService) {
		this.airPortService = airPortService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

}
