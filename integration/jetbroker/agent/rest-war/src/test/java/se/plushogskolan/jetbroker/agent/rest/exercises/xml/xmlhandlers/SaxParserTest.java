package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;

public class SaxParserTest {

	@Test
	public void testReadFlightRequestFromXmlUsingSax() throws Exception {

		FlightRequest fr = SaxParser
				.readFlightRequestFromXmlUsingSax("/flightRequest.xml");
		assertEquals("ID", 5, fr.getId());
		assertEquals("Departure airport", "GBG", fr.getDepartureAirportCode());
		assertEquals("Arrival airport", "STM", fr.getArrivalAirportCode());
		assertEquals("Status", FlightRequestStatus.CREATED,
				fr.getRequestStatus());
		assertEquals("No of passengers", 12, fr.getNoOfPassengers());

	}

}
