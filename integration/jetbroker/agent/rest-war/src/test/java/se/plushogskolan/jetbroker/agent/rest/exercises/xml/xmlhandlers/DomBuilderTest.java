package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import static org.junit.Assert.fail;

import org.junit.Test;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.XmlTestController;

public class DomBuilderTest {

	@Test
	public void buildXmlUsingDom() throws Exception {

		FlightRequest fr = XmlTestController.getMockedFlightRequest();
		String xml = DomBuilder.buildXmlUsingDom(fr);

		verifyXmlContains(xml, "<flightrequest id=\"5\">");
		verifyXmlContains(xml, "<arrivalAirportCode>AAA</arrivalAirportCode>");
		verifyXmlContains(xml,
				"<departureAirportCode>BBB</departureAirportCode>");
		verifyXmlContains(xml, "<noOfPassengers>10</noOfPassengers>");
		verifyXmlContains(xml, "<status>CREATED</status>");

	}

	public void verifyXmlContains(String xml, String textToMatch) {

		if (!xml.contains(textToMatch)) {
			fail("XML does not contain '" + textToMatch + "'");
		}

	}

}
