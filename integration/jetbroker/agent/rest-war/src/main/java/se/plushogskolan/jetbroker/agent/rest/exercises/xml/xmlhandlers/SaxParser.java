package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;

public class SaxParser {

	public static FlightRequest readFlightRequestFromXmlUsingSax(String file)
			throws Exception {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		InputStream stream = SaxParser.class.getResourceAsStream(file);

		MyHandler handler = new MyHandler();
		parser.parse(stream, handler);

		return handler.getFlightRequest();

	}

	static class MyHandler extends DefaultHandler {

		private FlightRequest request;
		private String currentTextValue;

		public FlightRequest getFlightRequest() {
			return request;
		}

		@Override
		public void startDocument() throws SAXException {
			request = new FlightRequest();
		}

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {

			if (qName.equals("flightrequest")) {
				request.setId(Long.valueOf(attributes.getValue("id")));
			}
		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			currentTextValue = new String(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			if (qName.equals("departureAirportCode")) {
				request.setDepartureAirportCode(currentTextValue);

			} else if (qName.equals("arrivalAirportCode")) {
				request.setArrivalAirportCode(currentTextValue);

			} else if (qName.equals("arrivalAirportCode")) {
				request.setRequestStatus(FlightRequestStatus
						.valueOf(currentTextValue));

			} else if (qName.equals("noOfPassengers")) {
				request.setNoOfPassengers(Integer.valueOf(currentTextValue));

			} else if (qName.equals("status")) {
				request.setRequestStatus(FlightRequestStatus
						.valueOf(currentTextValue));
			}
		}
	}
}
