package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import se.plushogskolan.jee.utils.xml.XmlUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class DomBuilder {

	public static String buildXmlUsingDom(FlightRequest fr) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();

		// root
		Element root = doc.createElement("flightrequest");
		root.setAttribute("id", String.valueOf(fr.getId()));
		doc.appendChild(root);

		// arrivalAirportCode
		Element arrivalAirportCode = doc.createElement("arrivalAirportCode");
		arrivalAirportCode.setTextContent(fr.getArrivalAirportCode());
		root.appendChild(arrivalAirportCode);

		// departureAirportCode
		Element departureAirportCode = doc
				.createElement("departureAirportCode");
		departureAirportCode.setTextContent(fr.getDepartureAirportCode());
		root.appendChild(departureAirportCode);

		// noOfPassengers
		Element noOfPassengers = doc.createElement("noOfPassengers");
		noOfPassengers.setTextContent(String.valueOf(fr.getNoOfPassengers()));
		root.appendChild(noOfPassengers);

		// status
		Element status = doc.createElement("status");
		status.setTextContent(fr.getRequestStatus().toString());
		root.appendChild(status);

		return XmlUtil.convertXmlDocumentToString(doc);

	}
}
