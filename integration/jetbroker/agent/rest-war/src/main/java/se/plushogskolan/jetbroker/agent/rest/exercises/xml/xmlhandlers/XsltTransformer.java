package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XsltTransformer {

	public static String transformUsingXslt(String xmlFile, String xlsFile)
			throws Exception {
		InputStream inXml = XsltTransformer.class.getResourceAsStream(xmlFile);
		InputStream inXsl = XsltTransformer.class.getResourceAsStream(xlsFile);

		StreamSource xslStream = new StreamSource(inXsl);
		StreamSource xmlStream = new StreamSource(inXml);
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(xslStream);
		StringWriter stringWriter = new StringWriter();
		StreamResult out = new StreamResult(stringWriter);

		transformer.transform(xmlStream, out);

		return stringWriter.getBuffer().toString();

	}
}
