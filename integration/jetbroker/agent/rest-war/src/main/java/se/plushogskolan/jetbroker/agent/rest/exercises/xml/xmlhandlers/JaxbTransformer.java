package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamResult;

import se.plushogskolan.jetbroker.agent.rest.exercises.xml.model.BoardingCard;

public class JaxbTransformer {

	public static String transformToXml(BoardingCard boardingCard)
			throws Exception {
		JAXBContext context = JAXBContext.newInstance(BoardingCard.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		StringWriter writer = new StringWriter();
		StreamResult streamResult = new StreamResult(writer);
		marshaller.marshal(boardingCard, streamResult);

		return writer.getBuffer().toString();

	}
}
