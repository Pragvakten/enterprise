package se.plushogskolan.jetbroker.agent.rest.customer;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.services.CustomerService;

@Controller
public class CustomerController {

	@Inject
	private CustomerService customerService;

	@ResponseBody
	@RequestMapping(value = "/createCustomer", method = RequestMethod.POST, produces = "application/json")
	public Customer createPlane(@RequestBody Customer customer)
			throws Exception {

		return customerService.createCustomer(customer);
	}

	@ResponseBody
	@RequestMapping(value = "/getCustomer/{id}", method = RequestMethod.GET, produces = "application/json")
	public Customer getPlane(@PathVariable long id) {
		return customerService.getCustomer(id);
	}

	@ResponseBody
	@RequestMapping(value = "/deleteCustomer/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletePlane(@PathVariable long id)
			throws Exception {

		customerService.deleteCustomer(id);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
