<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="flightrequest">
  <html>
  <body>
	<h1>Flight request - <xsl:value-of select="@id"/></h1>
	<p>Departure airport code: <xsl:value-of select="departureAirportCode"/></p>
	<p>arrivalAirportCode: <xsl:value-of select="arrivalAirportCode"/></p>
	<p>No of passengers: <xsl:value-of select="noOfPassengers"/></p>
	<p>Status: <xsl:value-of select="status"/></p>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet> 