package se.plushogskolan.jetbroker.agent.util;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged.ChangeType;
import se.plushogskolan.jetbroker.agent.events.FlightRequestChangedEvent;

/**
 * A class that listens to events for changes in FlightRequests.
 * 
 */
@ApplicationScoped
public class FlightRequestLogger {

	private int noOfFlightRequestConfirmations;
	private int noOfFlightRequestOffers;
	private int noOfFlightRequestRejections;

	Logger log = Logger.getLogger(FlightRequestLogger.class.getName());

	private void log() {

		log.info("*** FlightRequestLogger ***");
		log.info("No of flight request confirmations: "
				+ noOfFlightRequestConfirmations);
		log.info("No of flight request offers: " + noOfFlightRequestOffers);
		log.info("No of flight request rejections: "
				+ noOfFlightRequestRejections);
		log.info("***************************");

	}

	public void handleConfirmationEvent(
			@Observes @FlightRequestChanged(ChangeType.CONFIRMED) @Prod FlightRequestChangedEvent event) {

		noOfFlightRequestConfirmations++;
		log();
	}

	public void handleOfferEvent(
			@Observes @FlightRequestChanged(ChangeType.OFFER_RECEIVED) @Prod FlightRequestChangedEvent event) {
		noOfFlightRequestOffers++;
		log();
	}

	public void handleRejectionEvent(
			@Observes @FlightRequestChanged(ChangeType.REJECTED) @Prod FlightRequestChangedEvent event) {
		noOfFlightRequestRejections++;
		log();
	}

}
