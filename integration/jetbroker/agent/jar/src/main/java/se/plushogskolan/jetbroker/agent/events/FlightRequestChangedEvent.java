package se.plushogskolan.jetbroker.agent.events;

/**
 * An event that indicates that a FlightRequest has changed. This is a very generic event. Use the @FlightRequestChanged
 * annotation to give it more meaning.
 * 
 */
public class FlightRequestChangedEvent {

	private long flightRequestId;

	public FlightRequestChangedEvent(long flightRequestId) {
		setFlightRequestId(flightRequestId);
	}

	public long getFlightRequestId() {
		return flightRequestId;
	}

	public void setFlightRequestId(long flightRequestId) {
		this.flightRequestId = flightRequestId;
	}

}
