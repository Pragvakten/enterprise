package se.plushogskolan.jetbroker.agent.integration.plane.ws;

import java.io.ByteArrayOutputStream;
import java.util.Set;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * A SOAP Handler that can be used to intercept SOAP messages as they pass to and from webservices.
 *
 */
public class SoapListener implements SOAPHandler<SOAPMessageContext> {

	@Inject
	Logger log = Logger.getLogger(SoapListener.class.getName());

	@Override
	public void close(MessageContext arg0) {

	}

	@Override
	public boolean handleFault(SOAPMessageContext ctx) {
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext ctx) {
		try {
			SOAPMessage message = ctx.getMessage();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			message.writeTo(baos);
			log.info("SOAP message: " + baos.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

}
