package se.plushogskolan.jetbroker.agent.integration.order;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jetbroker.agent.services.AirPortService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:jboss/exported/jms/topic/planeBroadcastTopic"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'airportsChanged'") })
public class AirportsChangedMdb extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;
	@Inject
	private AirPortService airPortService;

	@Override
	public void onMessage(Message arg0) {
		log.fine("airportsChanged message recived: " + arg0);
		airPortService.handleAirportsChangedEvent();
	}
}
