package se.plushogskolan.jetbroker.agent.integration.order;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:jboss/exported/jms/queue/flightRequestResponseQueue"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'Confirmation'") })
public class FlightRequestConfirmationMdb extends AbstractMDB implements
		MessageListener {

	@Inject
	private FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) {
		MapMessage mapMessage = (MapMessage) message;
		try {
			long agentRequestId = mapMessage.getLong("agentId");
			long orderRequestId = mapMessage.getLong("orderId");

			FlightRequestConfirmation response = new FlightRequestConfirmation(
					agentRequestId, orderRequestId);
			flightRequestService.handleFlightRequestConfirmation(response);

		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
