package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.AirPort;

@Local
public interface AirPortRepository {

	List<AirPort> getAllAirPorts();

	AirPort getAirPort(String code);

	void handleAirportsChangedEvent();

}
