package se.plushogskolan.jetbroker.agent.integration.order;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBElement;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.order.xml.ObjectFactory;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequest;

@Prod
@Stateless
public class OrderFacadeImpl implements OrderFacade {

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	private QueueConnection connection;
	private QueueSession session;
	@Resource(mappedName = "java:jboss/exported/jms/queue/flightRequestQueue")
	private Queue queue;

	@Inject
	private Logger log;

	@Override
	public void sendFlightRequest(FlightRequest request) throws Exception {
		log.info("sendFlightRequest");
		connection = connectionFactory.createQueueConnection();
		session = connection
				.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

		QueueSender queueSender = session.createSender(queue);

		ObjectFactory factory = new ObjectFactory();
		XmlFlightRequest xmlFlightRequest = factory.createXmlFlightRequest();

		xmlFlightRequest.setAgentRequestId(request.getId());
		xmlFlightRequest.setDepartureAirportCode(request
				.getDepartureAirportCode());
		xmlFlightRequest.setArrivalAirportCode(request.getArrivalAirportCode());
		xmlFlightRequest.setNoOfPassengers(request.getNoOfPassengers());
		xmlFlightRequest.setDepartureTime(request.getDepartureTime()
				.getMillis());

		JAXBElement<XmlFlightRequest> element = factory
				.createFlightRequest(xmlFlightRequest);
		TextMessage textMessage = session.createTextMessage();
		textMessage.setStringProperty("messageType", "Created");
		textMessage.setText(JaxbUtil.generateXmlString(element,
				XmlFlightRequest.class));

		queueSender.send(textMessage);
		JmsHelper.closeConnectionAndSession(connection, session);
	}
}
