package se.plushogskolan.jetbroker.agent.integration.order;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequestOffer;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:jboss/exported/jms/queue/flightRequestResponseQueue"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'Offer'") })
public class FlightRequestOfferMdb extends AbstractMDB implements
		MessageListener {

	@Inject
	private FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) {

		TextMessage textMessage = (TextMessage) message;
		try {
			String xml = textMessage.getText();
			XmlFlightRequestOffer xmlOffer = (XmlFlightRequestOffer) JaxbUtil
					.generateJaxbObject(xml, XmlFlightRequestOffer.class);

			FlightOffer offer = new FlightOffer();
			offer.setOfferedPrice(xmlOffer.getOfferedPrice());
			offer.setPlaneTypeCode(xmlOffer.getPlaneTypeCode());
			flightRequestService
					.handleFlightOffer(offer, xmlOffer.getAgentId());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
