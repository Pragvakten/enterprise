package se.plushogskolan.jetbroker.agent.integration.order;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:jboss/exported/jms/queue/flightRequestResponseQueue"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'Rejection'") })
public class FlightRequestRejectionMdb extends AbstractMDB implements
		MessageListener {

	@Inject
	private FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) {
		MapMessage mapMessage = (MapMessage) message;

		try {
			flightRequestService.handleFlightRequestRejection(mapMessage
					.getLong("agentId"));
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
