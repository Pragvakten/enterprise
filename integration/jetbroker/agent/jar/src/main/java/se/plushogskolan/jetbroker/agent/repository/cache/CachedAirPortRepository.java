package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

@Stateless
public class CachedAirPortRepository implements AirPortRepository {

	private static final String AIRPORT_LIST = "airportList";
	private static final String AIRPORT_CACHE = "airportCache";

	@Inject
	Logger log;

	@Inject
	@Prod
	private PlaneFacade planeFacade;

	@Override
	public List<AirPort> getAllAirPorts() {
		CacheManager cacheManager = CacheManager.getInstance();
		Cache cache = cacheManager.getCache(AIRPORT_CACHE);
		log.fine("checking cache for airports");

		Element element = cache.get(AIRPORT_LIST);

		if (element != null) {
			List<AirPort> result = (List<AirPort>) element.getObjectValue();

			if (result != null) {
				log.fine("cache has been set, returning cached airports!");
				return result;
			}
		}
		log.fine("cache has not been set, caching airports");
		List<AirPort> result = planeFacade.getAllAirports();
		cache.put(new Element(AIRPORT_LIST, result));

		return result;
	}

	@Override
	public AirPort getAirPort(String code) {
		CacheManager cacheManager = CacheManager.getInstance();
		Cache cache = cacheManager.getCache(AIRPORT_CACHE);
		log.fine("Checking cache for airport: " + code);

		Element airportKey = cache.get(code);
		if (airportKey != null) {
			return (AirPort) airportKey.getObjectValue();
		}

		log.fine("Airport was not found in cache, checking planeFacade!");
		List<AirPort> list = planeFacade.getAllAirports();

		cache.put(new Element(AIRPORT_LIST, list));
		log.fine("New data cached");

		AirPort airport = findAirport(code, list);
		if (airport != null) {
			log.fine("airport found in planefacade");
			cache.put(new Element(airport.getCode(), airport));
			return airport;
		}

		log.fine("No airport found for code " + code);
		return null;

	}

	private AirPort findAirport(String code, List<AirPort> list) {
		for (AirPort airport : list) {
			if (airport.getCode().equals(code)) {
				return airport;
			}
		}
		return null;
	}

	@Override
	public void handleAirportsChangedEvent() {
		log.fine("Received airports changed event. ");
		log.fine("Dropping all cached airports!");
		CacheManager cacheManager = CacheManager.getInstance();
		Cache cache = cacheManager.getCache(AIRPORT_CACHE);
		cache.removeAll();
	}

}
