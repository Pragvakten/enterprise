package se.plushogskolan.jetbroker.agent.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

@Stateless
public class AirPortServiceImp implements AirPortService {

	@Inject
	private AirPortRepository airPortRepository;

	@Override
	public List<AirPort> getAllAirPorts() {
		return airPortRepository.getAllAirPorts();
	}

	@Override
	public AirPort getAirPort(String code) {
		return airPortRepository.getAirPort(code);
	}

	@Override
	public void handleAirportsChangedEvent() {
		// Implement
		airPortRepository.handleAirportsChangedEvent();
	}
}
