package se.plushogskolan.jetbroker.agent.domain.request;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;
import se.plushogskolan.jee.utils.validation.DifferentArrivalAndDepartureAirport;
import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * A flight request from the booking agent. The request is for a transportation
 * of x persons on a given date. The order system should respond to such a
 * request, first with a confirmation and later with an offer.
 * 
 */
@Entity
@DifferentArrivalAndDepartureAirport
public class FlightRequest implements IdHolder,
		ArrivalAndDepartureAirportHolder, Serializable {

	private static final long serialVersionUID = -5238688012184680573L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne
	@JoinColumn(name = "customerId")
	@NotNull
	private Customer customer;
	@Range(min = 1, max = 500)
	private int noOfPassengers;
	@NotBlank
	private String departureAirportCode;
	@NotBlank
	private String arrivalAirportCode;
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@NotNull
	private DateTime departureTime;
	private long confirmationId;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "offerId")
	private FlightOffer offer;
	@NotNull
	private FlightRequestStatus requestStatus;

	public String getDescription() {
		return " " + getDepartureAirportCode() + " - "
				+ getArrivalAirportCode();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public FlightOffer getOffer() {
		return offer;
	}

	public void setOffer(FlightOffer offer) {
		this.offer = offer;
	}

	public DateTime getDepartureTime() {
		return departureTime;
	}

	public Date getDepartureTimeAsDate() {
		return departureTime.toDate();
	}

	public void setDepartureTime(DateTime departureTime) {
		this.departureTime = departureTime;
	}

	public FlightRequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(FlightRequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public long getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(long confirmationId) {
		this.confirmationId = confirmationId;
	}

	@Override
	public String toString() {
		return "FlightRequest [id=" + id + ", customer=" + customer
				+ ", noOfPassengers=" + noOfPassengers
				+ ", departureAirportCode=" + departureAirportCode
				+ ", arrivalAirportCode=" + arrivalAirportCode
				+ ", departureTime=" + departureTime + ", confirmationId="
				+ confirmationId + ", offer=" + offer + ", requestStatus="
				+ requestStatus + "]";
	}

}
