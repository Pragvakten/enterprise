package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

@Stateless
public class CachedPlaneTypeRepository implements PlaneTypeRepository {

	private static final String PLANE_TYPE_CACHE = "planeTypeCache";
	private static final String PLANE_TYPE_LIST = "planeTypeList";

	@Inject
	Logger log;

	@Inject
	@Prod
	private PlaneFacade planeFacade;

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("getting all planeTypes");
		CacheManager cacheManager = CacheManager.getInstance();
		Cache cache = cacheManager.getCache(PLANE_TYPE_CACHE);

		log.fine("checking if cache has been set");
		Element element = cache.get(PLANE_TYPE_LIST);
		if (element != null) {
			List<PlaneType> types = (List<PlaneType>) element.getObjectValue();

			if (types != null) {
				log.fine("cache has been set, returning cached planeTypes!");
				return types;
			}
		}

		log.fine("cache has not been set, setting cache");
		List<PlaneType> result = planeFacade.getAllPlaneTypes();
		cache.put(new Element(PLANE_TYPE_LIST, result));

		return result;
	}

	@Override
	public PlaneType getPlaneType(String code) {
		CacheManager cacheManager = CacheManager.getInstance();
		Cache cache = cacheManager.getCache(PLANE_TYPE_CACHE);

		log.fine("Checking cache for planeType: " + code);
		Element planeTypeKey = cache.get(code);

		if (planeTypeKey != null) {
			return (PlaneType) planeTypeKey.getObjectValue();
		}

		log.fine("planeType not found in cache, checking facade");
		List<PlaneType> planeTypes = planeFacade.getAllPlaneTypes();
		cache.put(new Element(PLANE_TYPE_LIST, planeTypes));
		log.fine("New data cached");

		PlaneType planeType = findPlaneType(code, planeTypes);
		if (planeType != null) {
			log.fine("planeType found in facade");
			cache.put(new Element(planeType.getCode(), planeType));
			return planeType;
		}

		log.fine("No plane type found for code " + code);
		return null;
	}

	private PlaneType findPlaneType(String code, List<PlaneType> list) {
		for (PlaneType type : list) {
			if (type.getCode().equals(code)) {
				return type;
			}
		}
		return null;
	}

	@Override
	public void handlePlaneTypeChangedEvent() {
		log.fine("Handle plane types changed event");
		log.fine("dropping cached data");
		CacheManager cacheManager = CacheManager.getInstance();
		Cache cache = cacheManager.getCache(PLANE_TYPE_CACHE);
		cache.removeAll();
	}
}
