package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;

@Local
public interface CustomerRepository extends BaseRepository<Customer> {

	List<Customer> getAllCustomers();

}
