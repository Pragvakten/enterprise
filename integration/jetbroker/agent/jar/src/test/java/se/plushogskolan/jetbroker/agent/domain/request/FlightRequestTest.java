package se.plushogskolan.jetbroker.agent.domain.request;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Test;

import se.plushogskolan.jetbroker.agent.TestFixture;

public class FlightRequestTest {

	@Test
	public void testAirport_same() {
		FlightRequest req = TestFixture.getValidFlightRequest();
		req.setArrivalAirportCode("AAA");
		req.setDepartureAirportCode("AAA");
		Set<ConstraintViolation<FlightRequest>> violations = getValidator().validate(req);
		assertEquals("One error", 1, violations.size());

	}

	protected Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}
}
