package se.plushogskolan.jetbroker.agent.service;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.agent.services.FlightRequestServiceImp;

public class FlightRequestServiceTest {

	private FlightRequestServiceImp flightRequestService;
	private FlightRequestRepository repository;
	private OrderFacade facade;

	@Before
	public void setUp() throws Exception {
		flightRequestService = new FlightRequestServiceImp();
		repository = EasyMock.createMock(FlightRequestRepository.class);
		flightRequestService.setRepository(repository);

		facade = EasyMock.createMock(OrderFacade.class);
		flightRequestService.setOrderFacade(facade);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateFlightRequest() throws Exception {
		FlightRequest request = new FlightRequest();

		EasyMock.expect(repository.persist(request)).andReturn(new Long(1));
		EasyMock.expect(repository.findById(1)).andReturn(request);
		facade.sendFlightRequest(request);
		EasyMock.expectLastCall().once();

		EasyMock.replay(repository, facade);
		flightRequestService.createFlightRequest(request);
		EasyMock.verify(repository, facade);
	}

	@Test
	public void testHandleFlightRequestConfirmation() throws Exception {
		FlightRequestConfirmation response = new FlightRequestConfirmation(1, 2);
		FlightRequest request = new FlightRequest();

		EasyMock.expect(repository.findById(1)).andReturn(request);
		repository.update(request);
		EasyMock.expectLastCall().once();
		EasyMock.replay(repository);

		flightRequestService.handleFlightRequestConfirmation(response);
		EasyMock.verify(repository);
	}

	@Test
	public void testHandleFlightRequestRejection() throws Exception {
		FlightRequest request = new FlightRequest();

		EasyMock.expect(repository.findById(1)).andReturn(request);
		repository.update(EasyMock.isA(FlightRequest.class));
		EasyMock.expectLastCall().once();
		EasyMock.replay(repository);

		flightRequestService.handleFlightRequestRejection(1);
		EasyMock.verify(repository);

	}

	@Test
	public void testHandleOffer() throws Exception {
		FlightOffer offer = new FlightOffer();
		FlightRequest request = new FlightRequest();

		EasyMock.expect(repository.findById(1)).andReturn(request);
		repository.update(request);
		EasyMock.expectLastCall().once();
		EasyMock.replay(repository);

		flightRequestService.handleFlightOffer(offer, 1);
		EasyMock.verify(repository);
	}
}
