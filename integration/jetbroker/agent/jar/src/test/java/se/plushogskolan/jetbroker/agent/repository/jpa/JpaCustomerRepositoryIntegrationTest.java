package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

@RunWith(Arquillian.class)
public class JpaCustomerRepositoryIntegrationTest extends AbstractRepositoryTest<Customer, CustomerRepository> {

	private static Logger log = Logger.getLogger(JpaCustomerRepositoryIntegrationTest.class.getName());

	@Inject
	CustomerRepository customerRepo;

	@Override
	protected CustomerRepository getRepository() {
		return customerRepo;
	}

	@Override
	protected Customer getEntity1() {
		return TestFixture.getValidCustomer(0, "Kalle", "Andersson");
	}

	@Override
	protected Customer getEntity2() {
		return TestFixture.getValidCustomer(0, "Olle", "Persson");
	}

	@Test
	public void testGetAllCustomers() {

		customerRepo.persist(getEntity1());
		customerRepo.persist(getEntity2());
		List<Customer> customers = customerRepo.getAllCustomers();

		assertEquals("customers size: ", 2, customers.size());
	}

}
