package se.plushogskolan.jetbroker.plane.integration.airportws.mock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.airportws.AirportWebservice;

public class AirportWebserviceMock implements AirportWebservice {

	@Inject
	Logger log;

	@Override
	public Collection<Airport> getAllAirports() throws FailedIntegrationConnectionException {
		log.fine("MOCK: getAllAirports");
		List<Airport> airports = new ArrayList<Airport>();
		airports.add(new Airport(0, "GBG", "Gothenburg", 10, 20));
		airports.add(new Airport(0, "STM", "Stockholm", 30, 40));
		return airports;
	}

}
