package se.plushogskolan.jetbroker.plane.integration.airportws;

import java.util.Collection;

import se.plushogskolan.jetbroker.plane.domain.Airport;

/**
 * This is an interface towards en external webservice to fetch airports
 * 
 */
public interface AirportWebservice {

	public Collection<Airport> getAllAirports() throws Exception;

}
