package se.plushogskolan.jetbroker.plane.integration.jetbroker;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;

@Prod
@Stateless
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade {

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private TopicConnectionFactory connectionFactory;

	@Resource(mappedName = "java:jboss/exported/jms/topic/planeBroadcastTopic")
	private Topic topic;

	@Inject
	private Logger log;

	@Override
	public void broadcastNewFuelPrice(double fuelPrice) {
		log.fine("broadcastNewFuelPrice: " + fuelPrice);
		TopicConnection connection = null;
		TopicSession topicSession = null;

		try {
			connection = connectionFactory.createTopicConnection();
			topicSession = connection.createTopicSession(false,
					Session.AUTO_ACKNOWLEDGE);
			TopicPublisher topicPublisher = topicSession.createPublisher(topic);

			Message message = topicSession.createMessage();
			message.setDoubleProperty("fuelPrice", fuelPrice);
			message.setStringProperty("messageType", "FuelPriceChanged");

			topicPublisher.publish(message);

		} catch (JMSException e) {
			log.severe(e.getMessage());

		} finally {
			JmsHelper.closeConnectionAndSession(connection, topicSession);
		}
	}

	@Override
	public void broadcastAirportsChanged() {
		log.fine("broadcastAirportsChanged");
		TopicConnection connection = null;
		TopicSession topicSession = null;

		try {
			connection = connectionFactory.createTopicConnection();
			topicSession = connection.createTopicSession(false,
					Session.AUTO_ACKNOWLEDGE);
			TopicPublisher topicPublisher = topicSession.createPublisher(topic);

			Message message = topicSession.createMessage();
			message.setStringProperty("messageType", "airportsChanged");

			topicPublisher.publish(message);

		} catch (JMSException e) {
			log.severe(e.getMessage());

		} finally {
			JmsHelper.closeConnectionAndSession(connection, topicSession);
		}

	}

	@Override
	public void broadcastPlaneTypesChanged() {
		log.fine("broadcastPlaneTypesChanged");
		TopicConnection connection = null;
		TopicSession topicSession = null;

		try {
			connection = connectionFactory.createTopicConnection();
			topicSession = connection.createTopicSession(false,
					Session.AUTO_ACKNOWLEDGE);
			TopicPublisher topicPublisher = topicSession.createPublisher(topic);

			Message message = topicSession.createMessage();
			message.setStringProperty("messageType", "planeTypesChanged");

			topicPublisher.publish(message);

		} catch (JMSException e) {
			log.severe(e.getMessage());

		} finally {
			JmsHelper.closeConnectionAndSession(connection, topicSession);
		}

	}
}
