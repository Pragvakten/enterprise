package se.plushogskolan.jetbroker.plane.integration.airportws;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.domain.Airport;

@Stateless
@Prod
public class AirportWebServiceImpl implements AirportWebservice {

	@Inject
	private Logger log;

	@Override
	public Collection<Airport> getAllAirports() throws Exception {
		log.fine("Collection<Airport> getAllAirports()");

		se.plushogskolan.jetbroker.plane.integration.airportws.stubs.Airport port = new se.plushogskolan.jetbroker.plane.integration.airportws.stubs.Airport();
		String xml = port.getAirportSoap().getAirportInformationByCountry(
				"sweden");

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		MyHandler defaultHandler = new MyHandler();

		try {
			parser.parse(new InputSource(new StringReader(xml)), defaultHandler);

		} catch (Exception e) {
			log.severe(e.getMessage());
		}

		return defaultHandler.getAirports();
	}

	static class MyHandler extends DefaultHandler {

		private Set<Airport> airports;
		private String currentValue;
		private Airport airport;
		private boolean code;
		private boolean name;
		private boolean lat;
		private boolean lon;

		public List<Airport> getAirports() {
			return new ArrayList<Airport>(airports);
		}

		@Override
		public void startDocument() throws SAXException {
			airports = new HashSet<Airport>();
		}

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {

			if (qName.equals("Table")) {
				airport = new Airport();
			}

			if (qName.equals("AirportCode")) {
				code = true;
			}

			if (qName.equals("CityOrAirportName")) {
				name = true;
			}

			if (qName.equals("LatitudeDegree")) {
				lat = true;
			}

			if (qName.equals("LongitudeDegree")) {
				lon = true;
			}
		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {

			currentValue = new String(ch, start, length);

			if (name) {
				airport.setName(currentValue);
				name = false;
			}

			if (code) {
				airport.setCode(currentValue);
				code = false;
			}

			if (lon) {
				airport.setLongitude(Double.valueOf(currentValue));
				lon = false;
			}

			if (lat) {
				airport.setLatitude(Double.valueOf(currentValue));
				lat = false;
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {

			if (qName.equals("Table")) {
				airports.add(airport);
			}
		}
	}
}
