package se.plushogskolan.jetbroker.plane.domain.fuelprice;

import javax.ejb.Startup;
import javax.inject.Singleton;

@Startup
@Singleton
public class CachedFuelCost {

	private double cost;

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
}
