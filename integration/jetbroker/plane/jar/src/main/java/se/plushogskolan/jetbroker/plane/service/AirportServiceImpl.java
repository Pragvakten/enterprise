package se.plushogskolan.jetbroker.plane.service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.fuelprice.CachedFuelCost;
import se.plushogskolan.jetbroker.plane.integration.airportws.AirportWebservice;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	Logger log;

	@Inject
	@Prod
	private AirportWebservice airportWebservice;

	@Inject
	@Prod
	private PlaneIntegrationFacade facade;

	@Inject
	private CachedFuelCost fuelCost;

	@Inject
	private AirportRepository airportRepo;

	@Override
	public Airport getAirport(long id) {
		return getAirportRepo().findById(id);
	}

	@Override
	public Airport getAirportByCode(String code) {
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public void updateAirport(Airport airport) {
		getAirportRepo().update(airport);
		facade.broadcastAirportsChanged();
	}

	@Override
	public Airport createAirport(Airport airport) {
		long id = getAirportRepo().persist(airport);
		facade.broadcastAirportsChanged();
		return getAirport(id);
	}

	@Override
	public List<Airport> getAllAirports() {
		List<Airport> airports = getAirportRepo().getAllAirports();
		Collections.sort(airports);
		return airports;
	}

	@Override
	public double getFuelCost() {
		return fuelCost.getCost();
	}

	@Override
	public void updateFuelCost(double fuelCost) {
		this.fuelCost.setCost(fuelCost);
		facade.broadcastNewFuelPrice(fuelCost);
	}

	@Override
	public void deleteAirport(long id) {
		Airport airport = getAirport(id);
		getAirportRepo().remove(airport);
		facade.broadcastAirportsChanged();
	}

	@Override
	public void updateAirportsFromWebService() {
		// Implement
		log.info("updateAirportsFromWebService()");

		try {
			List<Airport> allWsAirports = (List<Airport>) airportWebservice
					.getAllAirports();
			List<Airport> allAirports = airportRepo.getAllAirports();

			for (Airport airport : allAirports) {
				airportRepo.remove(airport);
			}

			for (Airport airport : allWsAirports) {
				airportRepo.persist(airport);
			}

		} catch (Exception e) {
			log.severe(e.getMessage());
		}
	}

	public AirportRepository getAirportRepo() {
		return airportRepo;
	}

	public void setAirportRepo(AirportRepository airportRepo) {
		this.airportRepo = airportRepo;
	}

}
