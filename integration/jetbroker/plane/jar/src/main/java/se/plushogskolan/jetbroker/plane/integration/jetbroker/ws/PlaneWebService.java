package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import java.util.List;

import javax.jws.WebService;

@WebService
public interface PlaneWebService {

	public List<WSAirport> getAirports();

	public List<WSPlaneType> getPlaneTypes();

}
