package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

@Stateless
@WebService(endpointInterface = "se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService")
public class PlaneWebServiceImpl implements PlaneWebService {

	@Inject
	private PlaneService planeService;
	@Inject
	private AirportService airportService;
	@Inject
	private Logger log;

	@WebMethod
	@Override
	public List<WSAirport> getAirports() {
		log.fine("getAirports");
		List<Airport> airports = airportService.getAllAirports();
		List<WSAirport> result = new ArrayList<WSAirport>();

		for (Airport airport : airports) {
			WSAirport wsAirport = new WSAirport();
			wsAirport.setCode(airport.getCode());
			wsAirport.setLatitude(airport.getLatitude());
			wsAirport.setLongitude(airport.getLongitude());
			wsAirport.setName(airport.getName());
			result.add(wsAirport);
		}
		return result;
	}

	@WebMethod
	@Override
	public List<WSPlaneType> getPlaneTypes() {
		log.fine("getPlaneTypes");
		List<PlaneType> planeTypes = planeService.getAllPlaneTypes();
		List<WSPlaneType> result = new ArrayList<WSPlaneType>();

		for (PlaneType type : planeTypes) {
			WSPlaneType wsPlaneType = new WSPlaneType();
			wsPlaneType.setCode(type.getCode());
			wsPlaneType.setFuelConsumptionPerKm(type.getFuelConsumptionPerKm());
			wsPlaneType.setMaxNoOfPassengers(type.getMaxNoOfPassengers());
			wsPlaneType.setMaxRangeKm(type.getMaxRangeKm());
			wsPlaneType.setMaxSpeedKmH(type.getMaxSpeedKmH());
			wsPlaneType.setName(type.getName());
			result.add(wsPlaneType);
		}
		return result;
	}

}
