package se.plushogskolan.jetbroker.plane.mvc;

import javax.validation.Valid;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;

public class EditPlaneTypeBean {

	@Valid
	private PlaneType planeType;

	public PlaneType getPlaneType() {
		return planeType;
	}

	public void setPlaneType(PlaneType planeType) {
		this.planeType = planeType;
	}

}
