package se.plushogskolan.jetbroker.plane.mvc;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.service.AirportService;

/**
 * This controller stores all form fields in EditAirportBean. The negative
 * effect is that this duplicates the variables in Airport to EditAirportBean,
 * meaning more code. But it is also a straight forward and easy way of handling
 * form values. This can be especially suitable if the domain object has many
 * variables but we only want to update a few of them.
 * 
 * When showing the form, the controller copies the values from the domain
 * object to the bean. On submit, we copy the values from the bean to the domain
 * object.
 */
@Controller
@RequestMapping()
public class EditAirportController {

	private static Logger log = Logger.getLogger(EditAirportController.class
			.getName());

	@Inject
	private AirportService airportService;

	@RequestMapping(value = "/editAirport/{id}.html", method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		log.fine("Edit airport, id=" + id);

		EditAirportBean bean = new EditAirportBean();

		if (id > 0) {
			Airport airport = getAirportService().getAirport(id);
			bean.copyAirportValuesToBean(airport);
		}

		ModelAndView mav = new ModelAndView("editAirport");
		mav.addObject("editAirportBean", bean);
		return mav;
	}

	@RequestMapping(value = "/editAirport/{id}.html", method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditAirportBean bean,
			BindingResult errors) {

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editAirport");
			mav.addObject("editAirportBean", bean);
			return mav;
		}

		if (bean.getId() > 0) {
			Airport airport = getAirportService().getAirport(bean.getId());
			bean.copyBeanValuesToAirport(airport);
			getAirportService().updateAirport(airport);
		} else {
			Airport airport = new Airport();
			bean.copyBeanValuesToAirport(airport);
			getAirportService().createAirport(airport);
		}

		return new ModelAndView("redirect:/index.html");
	}

	@RequestMapping(value = "/deleteAirport/{id}.html", method = RequestMethod.GET)
	public ModelAndView deleteAirport(@PathVariable long id) {

		getAirportService().deleteAirport(id);

		return new ModelAndView("redirect:/index.html");
	}

	public AirportService getAirportService() {
		return airportService;
	}

	public void setAirportService(AirportService airportService) {
		this.airportService = airportService;
	}

}
