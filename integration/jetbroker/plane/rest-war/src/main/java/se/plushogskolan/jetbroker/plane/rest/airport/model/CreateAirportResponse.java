package se.plushogskolan.jetbroker.plane.rest.airport.model;

public class CreateAirportResponse {
	long id;

	public CreateAirportResponse(long id) {
		setId(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
