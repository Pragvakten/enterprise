package se.plushogskolan.jetbroker.plane.rest.planetype;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

@Controller
public class PlaneTypeController {

	@Inject
	private PlaneService planeService;

	@ResponseBody
	@RequestMapping(value = "/createPlaneType", method = RequestMethod.POST, produces = "application/json")
	public PlaneType createPlaneType(@RequestBody PlaneType planeType) {
		return planeService.createPlaneType(planeType);
	}

	@ResponseBody
	@RequestMapping(value = "/getPlaneType/{id}", method = RequestMethod.GET, produces = "application/json")
	public PlaneType getPlaneType(@PathVariable long id) {
		return planeService.getPlaneType(id);
	}

	@ResponseBody
	@RequestMapping(value = "/deletePlaneType/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletePlaneType(@PathVariable long id) {
		planeService.deletePlaneType(id);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
