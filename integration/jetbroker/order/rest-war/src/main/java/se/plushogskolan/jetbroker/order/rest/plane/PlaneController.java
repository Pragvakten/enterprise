package se.plushogskolan.jetbroker.order.rest.plane;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class PlaneController {

	@Inject
	private PlaneService planeService;

	@ResponseBody
	@RequestMapping(value = "/createPlane", method = RequestMethod.POST, produces = "application/json")
	public Plane createPlane(@RequestBody Plane plane) {
		return planeService.createPlane(plane);
	}

	@ResponseBody
	@RequestMapping(value = "/getPlane/{id}", method = RequestMethod.GET, produces = "application/json")
	public Plane getPlane(@PathVariable long id) {
		return planeService.getPlane(id);
	}

	@ResponseBody
	@RequestMapping(value = "/deletePlane/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletePlane(@PathVariable long id) {
		planeService.deletePlane(id);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
