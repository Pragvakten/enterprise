package se.plushogskolan.jetbroker.order;

import org.apache.log4j.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());

	public static FlightRequest getValidFlightRequest() {
		return getValidFlightRequest(1);
	}

	public static FlightRequest getValidFlightRequest(long id) {
		FlightRequest fr = new FlightRequest();
		fr.setId(id);
		fr.setAgentRequestId(10);
		fr.setArrivalAirportCode("GBG");
		fr.setDepartureAirportCode("STM");
		fr.setDistanceKm(122);
		fr.setNoOfPassengers(54);
		fr.setStatus(FlightRequestStatus.NEW);
		return fr;
	}

	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "order_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}

}
