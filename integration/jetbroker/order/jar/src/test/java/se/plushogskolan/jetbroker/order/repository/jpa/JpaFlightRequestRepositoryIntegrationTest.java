package se.plushogskolan.jetbroker.order.repository.jpa;

import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class JpaFlightRequestRepositoryIntegrationTest extends AbstractRepositoryTest<FlightRequest, FlightRequestRepository> {

	@Inject
	JpaFlightRequestRepository repository;

	@Override
	protected FlightRequestRepository getRepository() {
		return repository;
	}

	@Override
	protected FlightRequest getEntity1() throws Exception {
		return TestFixture.getValidFlightRequest(0);
	}

	@Override
	protected FlightRequest getEntity2() throws Exception {
		return TestFixture.getValidFlightRequest(0);
	}

}
