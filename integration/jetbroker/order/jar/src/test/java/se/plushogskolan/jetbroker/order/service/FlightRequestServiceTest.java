package se.plushogskolan.jetbroker.order.service;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceTest {

	private FlightRequestServiceImpl flightRequestService;
	private PlaneService planeService;
	private AgentIntegrationFacade agentIntegrationFacade;
	private FlightRequestRepository flightRequestRepository;

	@Before
	public void setUp() throws Exception {
		flightRequestService = new FlightRequestServiceImpl();

		planeService = EasyMock.createMock(PlaneService.class);
		flightRequestService.setPlaneService(planeService);

		agentIntegrationFacade = EasyMock
				.createMock(AgentIntegrationFacade.class);
		flightRequestService.setAgentIntegrationFacade(agentIntegrationFacade);

		flightRequestRepository = EasyMock
				.createMock(FlightRequestRepository.class);
		flightRequestService
				.setFlightRequestRepository(flightRequestRepository);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHandleFlightRequest() {
		FlightRequest request = new FlightRequest();
		request.setDepartureAirportCode("AAA");
		request.setArrivalAirportCode("AAA");

		EasyMock.expect(flightRequestRepository.persist(request)).andReturn(
				new Long(1));
		EasyMock.expect(planeService.getAirport("AAA"))
				.andReturn(new Airport());
		EasyMock.expect(planeService.getAirport("AAA"))
				.andReturn(new Airport());

		agentIntegrationFacade.sendFlightRequestConfirmation(EasyMock
				.isA(FlightRequestConfirmation.class));
		EasyMock.expectLastCall().once();

		EasyMock.replay(agentIntegrationFacade, flightRequestRepository,
				planeService);

		flightRequestService.handleIncomingFlightRequest(request);
		EasyMock.verify(agentIntegrationFacade, flightRequestRepository,
				planeService);
	}

	@Test
	public void testRejectFlightRequest() throws Exception {
		FlightRequest request = new FlightRequest();
		EasyMock.expect(flightRequestRepository.findById(1)).andReturn(request);
		flightRequestRepository.update(request);
		EasyMock.expectLastCall().once();
		EasyMock.replay(flightRequestRepository);

		flightRequestService.rejectFlightRequest(1);
		EasyMock.verify(flightRequestRepository);
	}

	@Test
	public void testHandleUpdatedOffer() throws Exception {
		Offer offer = new Offer();
		FlightRequest request = new FlightRequest();
		EasyMock.expect(flightRequestRepository.findById(1)).andReturn(request);
		flightRequestRepository.update(request);
		EasyMock.expectLastCall().once();
		agentIntegrationFacade.sendUpdatedOfferMessage(request);
		EasyMock.expectLastCall().once();

		EasyMock.replay(flightRequestRepository, agentIntegrationFacade);
		flightRequestService.handleUpdatedOffer(1, offer);
	}
}
