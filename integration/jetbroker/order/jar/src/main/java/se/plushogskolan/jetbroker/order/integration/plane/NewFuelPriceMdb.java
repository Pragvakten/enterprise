package se.plushogskolan.jetbroker.order.integration.plane;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:jboss/exported/jms/topic/planeBroadcastTopic"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "messageType = 'FuelPriceChanged'") })
public class NewFuelPriceMdb extends AbstractMDB implements MessageListener {

	@Inject
	private PlaneService planeService;
	@Inject
	private Logger log;

	@Override
	public void onMessage(Message message) {
		log.info("new fuelprice message recived: " + message);
		try {
			planeService.updateFuelCostPerLiter(message
					.getDoubleProperty("fuelPrice"));
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
