package se.plushogskolan.jetbroker.order.integration.agent;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBElement;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.xml.ObjectFactory;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequestOffer;

@Prod
@Stateless
public class AgentIntegrationFacadeImpl implements AgentIntegrationFacade {

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	private QueueConnection connection;
	private QueueSession session;
	@Resource(mappedName = "java:jboss/exported/jms/queue/flightRequestResponseQueue")
	private Queue queue;
	@Inject
	private Logger log;

	@Override
	public void sendUpdatedOfferMessage(FlightRequest flightRequest) {
		log.info("sendUpdatedOfferMessage: " + flightRequest);
		try {
			QueueSender queueSender = queueInit();

			ObjectFactory factory = new ObjectFactory();
			XmlFlightRequestOffer xmlFlightRequestOffer = factory
					.createXmlFlightRequestOffer();

			TextMessage textMessage = session.createTextMessage();
			xmlFlightRequestOffer.setAgentId(flightRequest.getAgentRequestId());
			xmlFlightRequestOffer.setOfferedPrice(flightRequest.getOffer()
					.getPrice());
			xmlFlightRequestOffer.setPlaneTypeCode(flightRequest.getOffer()
					.getPlane().getPlaneTypeCode());

			JAXBElement<XmlFlightRequestOffer> element = factory
					.createFlightRequestOffer(xmlFlightRequestOffer);
			textMessage.setText(JaxbUtil.generateXmlString(element,
					XmlFlightRequestOffer.class));
			textMessage.setStringProperty("messageType", "Offer");

			queueSender.send(textMessage);

		} catch (Exception e) {
			log.severe(e.getMessage());

		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}
	}

	@Override
	public void sendFlightRequestRejectedMessage(FlightRequest flightRequest) {
		log.info("sendFlightRequestRejectedMessage: " + flightRequest);
		try {
			QueueSender queueSender = queueInit();

			MapMessage mapMessage = session.createMapMessage();
			mapMessage.setLong("agentId", flightRequest.getAgentRequestId());
			mapMessage.setStringProperty("messageType", "Rejection");

			queueSender.send(mapMessage);

		} catch (JMSException e) {
			log.info(e.getMessage());

		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}

	}

	private QueueSender queueInit() throws JMSException {
		connection = connectionFactory.createQueueConnection();
		session = connection
				.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		QueueSender queueSender = session.createSender(queue);
		return queueSender;
	}

	@Override
	public void sendFlightRequestConfirmation(FlightRequestConfirmation response) {
		log.info("sendFlightRequestConfirmation: " + response);
		try {
			QueueSender queueSender = queueInit();

			MapMessage mapMessage = session.createMapMessage();
			mapMessage.setLong("agentId", response.getAgentRequestId());
			mapMessage.setLong("orderId", response.getOrderRequestId());
			mapMessage.setStringProperty("messageType", "Confirmation");

			queueSender.send(mapMessage);

		} catch (JMSException e) {
			log.severe(e.getMessage());

		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}

	}

}
