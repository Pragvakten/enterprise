package se.plushogskolan.jetbroker.order.integration.agent;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequest;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:jboss/exported/jms/queue/flightRequestQueue"),
		@ActivationConfigProperty(propertyName = "messageType", propertyValue = "Created") })
public class FlightRequestMdb extends AbstractMDB implements MessageListener {

	@Inject
	private FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) {
		TextMessage textMessage = (TextMessage) message;
		try {
			String xml = textMessage.getText();
			XmlFlightRequest xmlFlightRequest = (XmlFlightRequest) JaxbUtil
					.generateJaxbObject(xml, XmlFlightRequest.class);

			FlightRequest flightRequest = new FlightRequest();
			flightRequest.setAgentRequestId(xmlFlightRequest
					.getAgentRequestId());
			flightRequest.setDepartureAirportCode(xmlFlightRequest
					.getDepartureAirportCode());
			flightRequest.setArrivalAirportCode(xmlFlightRequest
					.getArrivalAirportCode());
			flightRequest.setDepartureTime(new DateTime(xmlFlightRequest
					.getDepartureTime()));
			flightRequest.setNoOfPassengers(xmlFlightRequest
					.getNoOfPassengers());

			flightRequestService.handleIncomingFlightRequest(flightRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
