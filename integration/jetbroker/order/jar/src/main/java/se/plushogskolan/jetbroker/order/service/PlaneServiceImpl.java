package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;

@Stateless
public class PlaneServiceImpl implements PlaneService {

	private static double cachedFuelPrice = 5.2;

	@Inject
	private AirportRepository airportRepository;

	@Inject
	private PlaneRepository planeRepository;

	@Inject
	private PlaneTypeRepository planeTypeRepository;

	@Override
	public Airport getAirport(String code) {
		return airportRepository.getAirport(code);
	}

	@Override
	public List<Plane> getAllPlanes() {
		return planeRepository.getAllPlanes();
	}

	@Override
	public Plane getPlane(long id) {
		return planeRepository.findById(id);
	}

	@Override
	public Plane createPlane(Plane plane) {
		long id = planeRepository.persist(plane);
		return planeRepository.findById(id);
	}

	@Override
	public void updatePlane(Plane plane) {
		planeRepository.update(plane);

	}

	@Override
	public PlaneType getPlaneType(String code) {
		return planeTypeRepository.getPlaneType(code);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return planeTypeRepository.getAllPlaneTypes();
	}

	@Override
	public double getFuelCostPerLiter() {
		return cachedFuelPrice;
	}

	@Override
	public void updateFuelCostPerLiter(double fuelCost) {
		cachedFuelPrice = fuelCost;

	}

	@Override
	public void deletePlane(long id) {
		Plane plane = getPlane(id);
		planeRepository.remove(plane);

	}

}
