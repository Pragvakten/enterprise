package se.plushogskolan.jetbroker.order.integration.agent.mock;

import java.util.logging.Logger;

import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;

public class AgentIntegrationFacadeMock implements AgentIntegrationFacade {

	@Inject
	Logger log;

	@Override
	public void sendUpdatedOfferMessage(FlightRequest flightRequest) {
		log.fine("MOCK: sendUpdatedOfferMessage: " + flightRequest);
	}

	@Override
	public void sendFlightRequestRejectedMessage(FlightRequest flightRequest) {
		log.fine("MOCK: sendFlightRequestRejectedMessage: " + flightRequest);
	}

	@Override
	public void sendFlightRequestConfirmation(FlightRequestConfirmation confirmation) {
		log.fine("MOCK: sendFlightRequestConfirmation: " + confirmation);

	}

}
