package se.plushogskolan.jetbroker.order.integration.plane;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsPlaneType;

@Stateless
@Prod
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade {

	@Inject
	private Logger log;

	@Override
	public List<Airport> getAllAirports() {
		log.fine("getAllAirports");

		PlaneWebService port = new PlaneWebServiceImplService()
				.getPort(PlaneWebService.class);
		List<WsAirport> wsAirports = port.getAirports();
		List<Airport> result = new ArrayList<Airport>();

		for (WsAirport wsAirport : wsAirports) {
			Airport airPort = new Airport();
			airPort.setCode(wsAirport.getCode());
			airPort.setName(wsAirport.getName());
			airPort.setLatitude(wsAirport.getLatitude());
			airPort.setLongitude(wsAirport.getLongitude());
			result.add(airPort);
		}
		return result;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("getAllPlaneTypes");

		PlaneWebService port = new PlaneWebServiceImplService()
				.getPort(PlaneWebService.class);
		List<WsPlaneType> wsPlaneTypes = port.getPlaneTypes();
		List<PlaneType> result = new ArrayList<PlaneType>();

		for (WsPlaneType wsPlaneType : wsPlaneTypes) {
			PlaneType planeType = new PlaneType();
			planeType.setCode(wsPlaneType.getCode());
			planeType.setFuelConsumptionPerKm(wsPlaneType
					.getFuelConsumptionPerKm());
			planeType.setMaxNoOfPassengers(wsPlaneType.getMaxNoOfPassengers());
			planeType.setMaxRangeKm(wsPlaneType.getMaxRangeKm());
			planeType.setMaxSpeedKmH(wsPlaneType.getMaxSpeedKmH());
			planeType.setName(wsPlaneType.getName());
			result.add(planeType);
		}
		return result;

	}

}
