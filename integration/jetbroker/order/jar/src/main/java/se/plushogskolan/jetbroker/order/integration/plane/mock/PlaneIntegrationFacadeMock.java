package se.plushogskolan.jetbroker.order.integration.plane.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.PlaneIntegrationFacade;

public class PlaneIntegrationFacadeMock implements PlaneIntegrationFacade {

	@Override
	public List<Airport> getAllAirports() {

		List<Airport> airports = new ArrayList<Airport>();
		airports.add(new Airport("GBG", "Gothenburg [Mocked]", 57.697, 11.98));
		airports.add(new Airport("STM", "Stockholm [Mocked]", 59.33, 18.06));
		airports.add(new Airport("AAA", "Airport 1", 10, 20));
		airports.add(new Airport("BBB", "Airport 2", 20, 30));

		return airports;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {

		List<PlaneType> types = new ArrayList<PlaneType>();

		PlaneType type1 = new PlaneType();
		type1.setCode("B770");
		type1.setName("Boeing 770 [Mocked]");
		type1.setFuelConsumptionPerKm(200);
		type1.setMaxNoOfPassengers(300);
		type1.setMaxRangeKm(700);
		type1.setMaxSpeedKmH(980);
		type1.setFuelConsumptionPerKm(34);
		types.add(type1);

		PlaneType type2 = new PlaneType();
		type2.setCode("A450");
		type2.setName("Airbus 450 [Mocked]");
		type2.setFuelConsumptionPerKm(140);
		type2.setMaxNoOfPassengers(270);
		type2.setMaxRangeKm(600);
		type2.setMaxSpeedKmH(680);
		type2.setFuelConsumptionPerKm(22);
		types.add(type2);

		return types;
	}

}
