<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

</head>
<body>
	<jsp:include page="header.jsp"/>

	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/plane.png">
		<c:choose>
			<c:when test="${isNew}">
				Create new plane
			</c:when>
			<c:otherwise>Edit plane</c:otherwise>
		</c:choose>
	</h2>

	<form:form commandName="editPlaneBean">
		<table class="formTable">
			<form:hidden path="planeId" />
			<c:if test="${not isNew}">
			<tr>
				<th>ID</th>
				<td>${editPlaneBean.planeId}</td>
				<td>
				</td>
			</tr>
			</c:if>
			<tr>
				<th>Plane type</th>
				<td><form:select path="planeTypeCode">
					<form:option value=""/>
					<form:options items="${planeTypes}" itemLabel="niceDescription" itemValue="code" />
				</form:select>  </td>
				<td>
				<form:errors path="planeTypeCode" cssClass="errors" />
				</td>
			</tr>
			<tr>
				<th>Description</th>
				<td><form:textarea path="description" rows="3" cols="50"/></td>
				<td><form:errors path="description" cssClass="errors" />
				</td>
			</tr>
			<tr>
				<th></th>
				<td colspan="2"><input type="submit" value="Submit" /> <a href="<%=request.getContextPath()%>">Cancel</a></td>
		</table>


	</form:form>

</body>

</html>