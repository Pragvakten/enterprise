package se.plushogskolan.jee.utils.xml;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

public class JaxbUtil {
	private static Logger log = Logger.getLogger(JaxbUtil.class.getName());

	public static String generateXmlString(JAXBElement<? extends Object> jaxbObject, Class clazz) throws Exception {

		JAXBContext ctx = JAXBContext.newInstance(clazz);
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
		StringWriter sw = new StringWriter();
		marshaller.marshal(jaxbObject, sw);
		log.fine("GenerateXML for class " + clazz + ": " + sw.toString());
		return sw.toString();

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object generateJaxbObject(String xml, Class clazz) throws Exception {
		JAXBContext ctx = JAXBContext.newInstance(clazz);
		Unmarshaller unmarshaller = ctx.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		JAXBElement jaxbElement = unmarshaller.unmarshal(new StreamSource(reader), clazz);
		return jaxbElement.getValue();

	}

}
