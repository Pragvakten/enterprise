package se.plushogskolan.jee.exercises.localization;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleExercise {

	public String sayHello(Locale locale) {

		return getString("hello", locale);

	}

	public String sayName(String name, Locale locale) {
		String text = getString("hello.name", locale);
		return MessageFormat.format(text, name);
	}

	public String saySomethingEnglish(Locale locale) {
		return getString("english.only", locale);
	}

	public String introduceYourself(int age, String city, String name,
			Locale locale) {
		String text = getString("introduction", locale);
		return MessageFormat.format(text, name, age, city);
	}

	public String sayApointmentDate(Date date, Locale locale) {
		String text = getString("appointment", locale);
		return new MessageFormat(text, locale).format(new Date[] { date });

	}

	protected String getString(String key, Locale locale) {
		return ResourceBundle.getBundle("localizationexercise", locale)
				.getString(key);
	}

	public String sayPrice(double price, Locale locale) {
		return NumberFormat.getCurrencyInstance(locale).format(price);
	}

	public String sayFractionDigits(double no) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumFractionDigits(3);
		return nf.format(no);
	}

}
