package se.plushogskolan.jee.utils.logging;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 * A CDI factory for producing Logger instances. This makes it easy to inject
 * Logger with
 * 
 * @Inject Logger log;
 * 
 *         instead of
 * 
 *         private static Logger log =
 *         Logger.getLogger(PlaneServiceImpl.class.getName());
 * 
 */
@ApplicationScoped
public class LoggingFactory {

	@Produces
	Logger createLogger(InjectionPoint injectionPoint) {

		return Logger.getLogger(injectionPoint.getMember().getDeclaringClass()
				.getName());

	}
}
