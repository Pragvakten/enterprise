package webservices.helloworld;

import java.net.MalformedURLException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * Implement the HelloWorld web service
 */
@WebService(endpointInterface = "webservices.helloworld.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

	public static void main(String[] args) throws MalformedURLException {
		// Implement so that this web service can start itself
		Endpoint.publish("http://localhost:8180/", new HelloWorldImpl());
	}

	@WebMethod
	public String helloWorld() {
		return "Hello world";
	}

}
