package webservices.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 * A handlerResolver that takes any number of handlers in the constructor.
 * 
 */
@SuppressWarnings("rawtypes")
public class MyHandlerResolver implements HandlerResolver {

	List<Handler> handlers = new ArrayList<Handler>();

	public MyHandlerResolver(Handler... handlers) {
		for (Handler handler : handlers) {
			this.handlers.add(handler);
		}
	}

	@Override
	public List<Handler> getHandlerChain(PortInfo arg0) {
		return handlers;
	}
}
