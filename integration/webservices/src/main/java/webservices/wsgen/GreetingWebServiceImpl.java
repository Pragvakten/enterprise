package webservices.wsgen;

import java.net.MalformedURLException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(endpointInterface = "webservices.wsgen.GreetingWebService")
public class GreetingWebServiceImpl implements GreetingWebService {

	public static void main(String[] args) throws MalformedURLException {
		// Implement so that this web service can start itself
		Endpoint.publish("http://localhost:8083/GreetingWebService",
				new GreetingWebServiceImpl());
	}

	@WebMethod
	public String greetSomeone(String name) {
		return "Hello " + name;
	}

}
