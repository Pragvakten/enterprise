package webservices.wsgen;

import javax.jws.WebService;

@WebService
public interface GreetingWebService {

	public String greetSomeone(String name);

}
