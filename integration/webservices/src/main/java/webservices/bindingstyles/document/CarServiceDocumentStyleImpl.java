package webservices.bindingstyles.document;

import java.net.MalformedURLException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import webservices.bindingstyles.Car;
import webservices.bindingstyles.CarRepository;
import webservices.bindingstyles.RegNumber;

/**
 * Implement the CarService web service.
 */
@WebService(endpointInterface = "webservices.bindingstyles.document.CarServiceDocumentStyle")
public class CarServiceDocumentStyleImpl implements CarServiceDocumentStyle {

	private CarRepository repo = new CarRepository();

	public static void main(String[] args) throws MalformedURLException {
		// Implement so that this web service can start itself
		Endpoint.publish("http://localhost:8180/doc",
				new CarServiceDocumentStyleImpl());
	}

	@Override
	@WebMethod
	public Car getCar(RegNumber regNo) {
		return repo.getCar(regNo);
	}

}
