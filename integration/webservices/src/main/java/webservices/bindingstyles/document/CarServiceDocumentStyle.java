package webservices.bindingstyles.document;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import webservices.bindingstyles.CarService;

/**
 * Add annotations to make this a document style web service
 */
@WebService
@SOAPBinding(style = Style.DOCUMENT)
public interface CarServiceDocumentStyle extends CarService {

}
